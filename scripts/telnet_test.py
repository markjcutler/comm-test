#!/usr/bin/env python

###################################################
# telnet_test.py -- test telnet'ing into a wifly
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Friday,  5 May 2015.
###################################################


# import telnetlib
# hostserver = "192.168.0.132"
# newline = "\n"
# telnet = telnetlib.Telnet(hostserver, "30057")
# while 1:
#     command = raw_input("[shell]: ")
#     print "here1"
#     telnet.write(command)
#     print "here2"
#     if command == "exit":
#         break
#     #telnet.read_all()
#     print "here3"

# telnet program example
import socket, select, string, sys
  
#main function
if __name__ == "__main__":
      
    if(len(sys.argv) < 3) :
        print 'Usage : python telnet.py hostname port'
        sys.exit()
      
    host = sys.argv[1]
    port = int(sys.argv[2])
      
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(2)
      
    # connect to remote host
    try :
        s.connect((host, port))
    except :
        print 'Unable to connect'
        sys.exit()
      
    print 'Connected to remote host'
    
    first_data = True
      
    while 1:
        socket_list = [sys.stdin, s]
          
        # Get the list sockets which are readable
        read_sockets, write_sockets, error_sockets = select.select(socket_list , [], [])
          
        for sock in read_sockets:
            #incoming message from remote server
            if sock == s:
                data = sock.recv(4096)
                if not data :
                    print 'Connection closed'
                    sys.exit()
                else :
                    print data
#                     if first_data:
#                         sys.stdout.write(data)
#                         first_data = False
#                     else:
#                         sys.stdout.write(data + '\n')
              
            #user entered a message
            else :
                msg = sys.stdin.readline()
                #msg = sys.stdin.read(1)
                #print msg
                
                if first_data:
                    s.send(msg)
                    first_data = False
                else:
                    s.send(msg + '\r\n')
