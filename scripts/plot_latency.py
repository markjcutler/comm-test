#!/usr/bin/env python

###################################################
# plot_latency.py -- plot latency from a bag file
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Friday,  2 January 2015.
###################################################

import numpy as np
import matplotlib.pyplot as plt
import os
import csv
from comm_test.msg import CommStats


#bag_file = '/home/mark/test_2015-01-02-15-43-52.bag'


latency = []
dropped_packets = []

os.system('rostopic echo -b /home/mark/test_2015-01-02-15-43-52.bag -p /comm_stats > output.txt')
first_row = False
with open('output.txt', 'r') as csvfile:
    data = csv.reader(csvfile)
    for row in data:
        if not first_row:
            first_row = True
        else:
            latency.append(float(row[1]))
            dropped_packets.append(int(row[2]))

mean_latency = np.mean(latency)
std_dev_latency = np.std(latency)
print mean_latency
print std_dev_latency

cs = CommStats()
print cs.latency
