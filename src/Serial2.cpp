/*
 * Serail2.cpp
 *
 *  Created on: Jan 5, 2015
 *      Author: mark
 */

#include "Serial2.h"

Serial2::Serial2(int baudRate, std::string serialPort1, std::string serialPort2)
{
    p1_init = false;
    p2_init = false;

    if (!ser1.spInitialize(serialPort1.c_str(), baudRate, true))
    {
        ROS_ERROR_STREAM("Serial port 1 failed to open at " << baudRate << " baud rate.");
        return;
    }

    p1_init = true;

    if (!ser2.spInitialize(serialPort2.c_str(), baudRate, true))
    {
        ROS_ERROR_STREAM("Serial port 2 failed to open at " << baudRate << " baud rate.");
        return;
    }

    p2_init = true;
}

Serial2::~Serial2()
{
    // TODO Auto-generated destructor stub
}

int Serial2::get_data(uint8_t *buf, int len)
{
    if (len > 0)
        buf[0] = ser2.spReceiveSingle();
    else
        return 0;
    return 1;
}

int Serial2::send_data(uint8_t *buf , int len)
{
    return ser1.spSend(buf, len);
}
