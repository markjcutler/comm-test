/*
  ip_address.cpp -- Get all current ip addresses on this machine
   *
    * Created on Friday, 13 March 2015.
    */

#include <iostream>
#include <sstream>
#include <stdio.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>



bool get_acl_ip(int expected_address[], int subnet_length,
                std::string &acl_raven_ip)
{
    struct ifaddrs * ifAddrStruct=NULL;
    struct ifaddrs * ifa=NULL;
    void * tmpAddrPtr=NULL;

    getifaddrs(&ifAddrStruct);

    bool found_acl_ip = true;
    printf("\n\n\nLooking for all IP addresses on this machine\n\n\n");
    for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
        if (!ifa->ifa_addr) {
            continue;
        }
        if (ifa->ifa_addr->sa_family == AF_INET) { // check it is IP4
            // is a valid IP4 Address
            tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
            char addressBuffer[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
            printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer);

            // parse out the ip4 address into a series of four numbers
            std::string ab(addressBuffer);
            std::istringstream iss(ab);
            std::string token;
            int cnt = 0;
            found_acl_ip = true;
            while (std::getline(iss, token, '.')) {
                if (!token.empty() and cnt < subnet_length)
                {
                    int add_num = atoi(token.c_str());
                    if (add_num != expected_address[cnt])
                        found_acl_ip = false;
                    cnt++;
                }
            }

            if (found_acl_ip)
                acl_raven_ip = addressBuffer;

        }//  else if (ifa->ifa_addr->sa_family == AF_INET6) { // check it is IP6
        //     // is a valid IP6 Address
        //     tmpAddrPtr=&((struct sockaddr_in6 *)ifa->ifa_addr)->sin6_addr;
        //     char addressBuffer[INET6_ADDRSTRLEN];
        //     inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
        //     printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer);
        // }
    }
    if (ifAddrStruct!=NULL) freeifaddrs(ifAddrStruct);

    if (found_acl_ip)
        printf("\n\n\nFound the acl-raven-ip: %s\n\n\n", acl_raven_ip.c_str());
    else
        printf("\n\n\nNo acl-raven-ip address found\n\n\n");
    return found_acl_ip;
}



int main (int argc, const char * argv[]) {
    std::string acl_raven_ip;
    int expected_address[3] = {192, 168, 0};

    std::cout << get_acl_ip(expected_address,
                            sizeof(expected_address)/sizeof(expected_address[0]),
                            acl_raven_ip) << std::endl;

    return 0;
}
