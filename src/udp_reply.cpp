/*
 * reply.cpp
 *
 *  Created on: Dec 22, 2014
 *      Author: mark
 */


#include <iostream>

// ROS includes
#include "ros/ros.h"

// acl utils library
#include "acl/comms/udpPort.hpp"
#include "acl/utils.hpp"

/* This assumes you have the mavlink headers on your include path
 or in the same folder as this source file */
#include "mavlink_msgs/acl_msgs/mavlink.h"

#define BUFFER_LENGTH 2046 // minimum buffer size that can be used with qnx (I don't know why)


int main(int argc, char* argv[])
{

	// initialize ros node
	ros::init(argc, argv, "reply");
	ros::NodeHandle n;

	uint8_t buf[BUFFER_LENGTH];
	ssize_t recsize;
	socklen_t fromlen;
	int bytes_sent;
	mavlink_message_t msg;
	uint16_t len;

	// setup udp communication
	char *address = "127.0.0.1";
	int rxport = 30052;
	int txport = 20052;
	acl::UDPPort udp;
	if (!udp.udpInit(true, address, rxport, txport))
	{
		ROS_ERROR("UDP port failed to open");
		return 0;
	}

        int packet_drops = 0;
        int last_seq = -1;
        long long message_num = 0;

	while(ros::ok())
	{

		mavlink_message_t msg;
		mavlink_status_t status;

		// COMMUNICATION THROUGH EXTERNAL UART PORT (XBee serial)

		char bufr[2048];
		int received_bytes = udp.udpReceive(bufr, sizeof(bufr));
		// Try to get a new message
		for (int i=0; i<received_bytes; i++)
		{
			uint8_t c = bufr[i];
			// Try to get a new message
			if (mavlink_parse_char(MAVLINK_COMM_0, c, &msg, &status))
			{
				// Handle message

            // Handle message
            mavlink_latency_test_t latency;
            switch (msg.msgid)
            {
            case MAVLINK_MSG_ID_LATENCY_TEST:
            {

                mavlink_msg_latency_test_decode(&msg, &latency);

                packet_drops += (uint8_t) (latency.fill1 - last_seq - 1);
                last_seq = latency.fill1;

                double lat = ros::Time::now().toSec() - latency.send_time;

                message_num++;
                if (message_num % 20 == 0)
                    std::cout << "Got message number " << latency.fill1 << ". Dropped packets: " << packet_drops << ", Latency: " << lat << std::endl;


                mavlink_msg_latency_test_decode(&msg, &latency);

                /*Send Heartbeat */
                uint8_t buf[BUFFER_LENGTH];
                mavlink_message_t msg2;
                mavlink_msg_latency_test_pack(1, 200, &msg2, latency.send_time, latency.fill1, 2, 3);
                uint16_t len = mavlink_msg_to_send_buffer(buf, &msg2);
                bytes_sent = udp.udpSend(buf, len);
            }
            break;
            default:
                //Do nothing
                break;
            }

			}
		}



	}

	return 1;
}
