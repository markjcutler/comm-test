// MESSAGE ATT_CMD_MEASURE PACKING

#define MAVLINK_MSG_ID_ATT_CMD_MEASURE 150

typedef struct __mavlink_att_cmd_measure_t
{
 float p_cmd; ///< Commanded roll rate.
 float q_cmd; ///< Commanded pitch rate.
 float r_cmd; ///< Commanded yaw rate.
 float qo_cmd; ///< Commanded quaternion 'o' value.
 float qx_cmd; ///< Commanded quaternion 'x' value.
 float qy_cmd; ///< Commanded quaternion 'y' value.
 float qz_cmd; ///< Commanded quaternion 'z' value.
 float qo_meas; ///< Measured quaternion 'o' value.
 float qx_meas; ///< Measured quaternion 'x' value.
 float qy_meas; ///< Measured quaternion 'y' value.
 float qz_meas; ///< Measured quaternion 'z' value.
 uint8_t att_cmd; ///< Control mode.  Normal flight is 1.  Motors are shut off with 0.
 uint8_t throttle; ///< Total thrust applied evenly to the 4 motors.
} mavlink_att_cmd_measure_t;

#define MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN 46
#define MAVLINK_MSG_ID_150_LEN 46

#define MAVLINK_MSG_ID_ATT_CMD_MEASURE_CRC 84
#define MAVLINK_MSG_ID_150_CRC 84



#define MAVLINK_MESSAGE_INFO_ATT_CMD_MEASURE { \
	"ATT_CMD_MEASURE", \
	13, \
	{  { "p_cmd", NULL, MAVLINK_TYPE_FLOAT, 0, 0, offsetof(mavlink_att_cmd_measure_t, p_cmd) }, \
         { "q_cmd", NULL, MAVLINK_TYPE_FLOAT, 0, 4, offsetof(mavlink_att_cmd_measure_t, q_cmd) }, \
         { "r_cmd", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_att_cmd_measure_t, r_cmd) }, \
         { "qo_cmd", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_att_cmd_measure_t, qo_cmd) }, \
         { "qx_cmd", NULL, MAVLINK_TYPE_FLOAT, 0, 16, offsetof(mavlink_att_cmd_measure_t, qx_cmd) }, \
         { "qy_cmd", NULL, MAVLINK_TYPE_FLOAT, 0, 20, offsetof(mavlink_att_cmd_measure_t, qy_cmd) }, \
         { "qz_cmd", NULL, MAVLINK_TYPE_FLOAT, 0, 24, offsetof(mavlink_att_cmd_measure_t, qz_cmd) }, \
         { "qo_meas", NULL, MAVLINK_TYPE_FLOAT, 0, 28, offsetof(mavlink_att_cmd_measure_t, qo_meas) }, \
         { "qx_meas", NULL, MAVLINK_TYPE_FLOAT, 0, 32, offsetof(mavlink_att_cmd_measure_t, qx_meas) }, \
         { "qy_meas", NULL, MAVLINK_TYPE_FLOAT, 0, 36, offsetof(mavlink_att_cmd_measure_t, qy_meas) }, \
         { "qz_meas", NULL, MAVLINK_TYPE_FLOAT, 0, 40, offsetof(mavlink_att_cmd_measure_t, qz_meas) }, \
         { "att_cmd", NULL, MAVLINK_TYPE_UINT8_T, 0, 44, offsetof(mavlink_att_cmd_measure_t, att_cmd) }, \
         { "throttle", NULL, MAVLINK_TYPE_UINT8_T, 0, 45, offsetof(mavlink_att_cmd_measure_t, throttle) }, \
         } \
}


/**
 * @brief Pack a att_cmd_measure message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param att_cmd Control mode.  Normal flight is 1.  Motors are shut off with 0.
 * @param throttle Total thrust applied evenly to the 4 motors.
 * @param p_cmd Commanded roll rate.
 * @param q_cmd Commanded pitch rate.
 * @param r_cmd Commanded yaw rate.
 * @param qo_cmd Commanded quaternion 'o' value.
 * @param qx_cmd Commanded quaternion 'x' value.
 * @param qy_cmd Commanded quaternion 'y' value.
 * @param qz_cmd Commanded quaternion 'z' value.
 * @param qo_meas Measured quaternion 'o' value.
 * @param qx_meas Measured quaternion 'x' value.
 * @param qy_meas Measured quaternion 'y' value.
 * @param qz_meas Measured quaternion 'z' value.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_att_cmd_measure_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
						       uint8_t att_cmd, uint8_t throttle, float p_cmd, float q_cmd, float r_cmd, float qo_cmd, float qx_cmd, float qy_cmd, float qz_cmd, float qo_meas, float qx_meas, float qy_meas, float qz_meas)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN];
	_mav_put_float(buf, 0, p_cmd);
	_mav_put_float(buf, 4, q_cmd);
	_mav_put_float(buf, 8, r_cmd);
	_mav_put_float(buf, 12, qo_cmd);
	_mav_put_float(buf, 16, qx_cmd);
	_mav_put_float(buf, 20, qy_cmd);
	_mav_put_float(buf, 24, qz_cmd);
	_mav_put_float(buf, 28, qo_meas);
	_mav_put_float(buf, 32, qx_meas);
	_mav_put_float(buf, 36, qy_meas);
	_mav_put_float(buf, 40, qz_meas);
	_mav_put_uint8_t(buf, 44, att_cmd);
	_mav_put_uint8_t(buf, 45, throttle);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN);
#else
	mavlink_att_cmd_measure_t packet;
	packet.p_cmd = p_cmd;
	packet.q_cmd = q_cmd;
	packet.r_cmd = r_cmd;
	packet.qo_cmd = qo_cmd;
	packet.qx_cmd = qx_cmd;
	packet.qy_cmd = qy_cmd;
	packet.qz_cmd = qz_cmd;
	packet.qo_meas = qo_meas;
	packet.qx_meas = qx_meas;
	packet.qy_meas = qy_meas;
	packet.qz_meas = qz_meas;
	packet.att_cmd = att_cmd;
	packet.throttle = throttle;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_ATT_CMD_MEASURE;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN, MAVLINK_MSG_ID_ATT_CMD_MEASURE_CRC);
#else
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN);
#endif
}

/**
 * @brief Pack a att_cmd_measure message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param att_cmd Control mode.  Normal flight is 1.  Motors are shut off with 0.
 * @param throttle Total thrust applied evenly to the 4 motors.
 * @param p_cmd Commanded roll rate.
 * @param q_cmd Commanded pitch rate.
 * @param r_cmd Commanded yaw rate.
 * @param qo_cmd Commanded quaternion 'o' value.
 * @param qx_cmd Commanded quaternion 'x' value.
 * @param qy_cmd Commanded quaternion 'y' value.
 * @param qz_cmd Commanded quaternion 'z' value.
 * @param qo_meas Measured quaternion 'o' value.
 * @param qx_meas Measured quaternion 'x' value.
 * @param qy_meas Measured quaternion 'y' value.
 * @param qz_meas Measured quaternion 'z' value.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_att_cmd_measure_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
							   mavlink_message_t* msg,
						           uint8_t att_cmd,uint8_t throttle,float p_cmd,float q_cmd,float r_cmd,float qo_cmd,float qx_cmd,float qy_cmd,float qz_cmd,float qo_meas,float qx_meas,float qy_meas,float qz_meas)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN];
	_mav_put_float(buf, 0, p_cmd);
	_mav_put_float(buf, 4, q_cmd);
	_mav_put_float(buf, 8, r_cmd);
	_mav_put_float(buf, 12, qo_cmd);
	_mav_put_float(buf, 16, qx_cmd);
	_mav_put_float(buf, 20, qy_cmd);
	_mav_put_float(buf, 24, qz_cmd);
	_mav_put_float(buf, 28, qo_meas);
	_mav_put_float(buf, 32, qx_meas);
	_mav_put_float(buf, 36, qy_meas);
	_mav_put_float(buf, 40, qz_meas);
	_mav_put_uint8_t(buf, 44, att_cmd);
	_mav_put_uint8_t(buf, 45, throttle);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN);
#else
	mavlink_att_cmd_measure_t packet;
	packet.p_cmd = p_cmd;
	packet.q_cmd = q_cmd;
	packet.r_cmd = r_cmd;
	packet.qo_cmd = qo_cmd;
	packet.qx_cmd = qx_cmd;
	packet.qy_cmd = qy_cmd;
	packet.qz_cmd = qz_cmd;
	packet.qo_meas = qo_meas;
	packet.qx_meas = qx_meas;
	packet.qy_meas = qy_meas;
	packet.qz_meas = qz_meas;
	packet.att_cmd = att_cmd;
	packet.throttle = throttle;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_ATT_CMD_MEASURE;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN, MAVLINK_MSG_ID_ATT_CMD_MEASURE_CRC);
#else
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN);
#endif
}

/**
 * @brief Encode a att_cmd_measure struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param att_cmd_measure C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_att_cmd_measure_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_att_cmd_measure_t* att_cmd_measure)
{
	return mavlink_msg_att_cmd_measure_pack(system_id, component_id, msg, att_cmd_measure->att_cmd, att_cmd_measure->throttle, att_cmd_measure->p_cmd, att_cmd_measure->q_cmd, att_cmd_measure->r_cmd, att_cmd_measure->qo_cmd, att_cmd_measure->qx_cmd, att_cmd_measure->qy_cmd, att_cmd_measure->qz_cmd, att_cmd_measure->qo_meas, att_cmd_measure->qx_meas, att_cmd_measure->qy_meas, att_cmd_measure->qz_meas);
}

/**
 * @brief Encode a att_cmd_measure struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param att_cmd_measure C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_att_cmd_measure_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_att_cmd_measure_t* att_cmd_measure)
{
	return mavlink_msg_att_cmd_measure_pack_chan(system_id, component_id, chan, msg, att_cmd_measure->att_cmd, att_cmd_measure->throttle, att_cmd_measure->p_cmd, att_cmd_measure->q_cmd, att_cmd_measure->r_cmd, att_cmd_measure->qo_cmd, att_cmd_measure->qx_cmd, att_cmd_measure->qy_cmd, att_cmd_measure->qz_cmd, att_cmd_measure->qo_meas, att_cmd_measure->qx_meas, att_cmd_measure->qy_meas, att_cmd_measure->qz_meas);
}

/**
 * @brief Send a att_cmd_measure message
 * @param chan MAVLink channel to send the message
 *
 * @param att_cmd Control mode.  Normal flight is 1.  Motors are shut off with 0.
 * @param throttle Total thrust applied evenly to the 4 motors.
 * @param p_cmd Commanded roll rate.
 * @param q_cmd Commanded pitch rate.
 * @param r_cmd Commanded yaw rate.
 * @param qo_cmd Commanded quaternion 'o' value.
 * @param qx_cmd Commanded quaternion 'x' value.
 * @param qy_cmd Commanded quaternion 'y' value.
 * @param qz_cmd Commanded quaternion 'z' value.
 * @param qo_meas Measured quaternion 'o' value.
 * @param qx_meas Measured quaternion 'x' value.
 * @param qy_meas Measured quaternion 'y' value.
 * @param qz_meas Measured quaternion 'z' value.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_att_cmd_measure_send(mavlink_channel_t chan, uint8_t att_cmd, uint8_t throttle, float p_cmd, float q_cmd, float r_cmd, float qo_cmd, float qx_cmd, float qy_cmd, float qz_cmd, float qo_meas, float qx_meas, float qy_meas, float qz_meas)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN];
	_mav_put_float(buf, 0, p_cmd);
	_mav_put_float(buf, 4, q_cmd);
	_mav_put_float(buf, 8, r_cmd);
	_mav_put_float(buf, 12, qo_cmd);
	_mav_put_float(buf, 16, qx_cmd);
	_mav_put_float(buf, 20, qy_cmd);
	_mav_put_float(buf, 24, qz_cmd);
	_mav_put_float(buf, 28, qo_meas);
	_mav_put_float(buf, 32, qx_meas);
	_mav_put_float(buf, 36, qy_meas);
	_mav_put_float(buf, 40, qz_meas);
	_mav_put_uint8_t(buf, 44, att_cmd);
	_mav_put_uint8_t(buf, 45, throttle);

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ATT_CMD_MEASURE, buf, MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN, MAVLINK_MSG_ID_ATT_CMD_MEASURE_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ATT_CMD_MEASURE, buf, MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN);
#endif
#else
	mavlink_att_cmd_measure_t packet;
	packet.p_cmd = p_cmd;
	packet.q_cmd = q_cmd;
	packet.r_cmd = r_cmd;
	packet.qo_cmd = qo_cmd;
	packet.qx_cmd = qx_cmd;
	packet.qy_cmd = qy_cmd;
	packet.qz_cmd = qz_cmd;
	packet.qo_meas = qo_meas;
	packet.qx_meas = qx_meas;
	packet.qy_meas = qy_meas;
	packet.qz_meas = qz_meas;
	packet.att_cmd = att_cmd;
	packet.throttle = throttle;

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ATT_CMD_MEASURE, (const char *)&packet, MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN, MAVLINK_MSG_ID_ATT_CMD_MEASURE_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ATT_CMD_MEASURE, (const char *)&packet, MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN);
#endif
#endif
}

#if MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_att_cmd_measure_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t att_cmd, uint8_t throttle, float p_cmd, float q_cmd, float r_cmd, float qo_cmd, float qx_cmd, float qy_cmd, float qz_cmd, float qo_meas, float qx_meas, float qy_meas, float qz_meas)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char *buf = (char *)msgbuf;
	_mav_put_float(buf, 0, p_cmd);
	_mav_put_float(buf, 4, q_cmd);
	_mav_put_float(buf, 8, r_cmd);
	_mav_put_float(buf, 12, qo_cmd);
	_mav_put_float(buf, 16, qx_cmd);
	_mav_put_float(buf, 20, qy_cmd);
	_mav_put_float(buf, 24, qz_cmd);
	_mav_put_float(buf, 28, qo_meas);
	_mav_put_float(buf, 32, qx_meas);
	_mav_put_float(buf, 36, qy_meas);
	_mav_put_float(buf, 40, qz_meas);
	_mav_put_uint8_t(buf, 44, att_cmd);
	_mav_put_uint8_t(buf, 45, throttle);

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ATT_CMD_MEASURE, buf, MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN, MAVLINK_MSG_ID_ATT_CMD_MEASURE_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ATT_CMD_MEASURE, buf, MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN);
#endif
#else
	mavlink_att_cmd_measure_t *packet = (mavlink_att_cmd_measure_t *)msgbuf;
	packet->p_cmd = p_cmd;
	packet->q_cmd = q_cmd;
	packet->r_cmd = r_cmd;
	packet->qo_cmd = qo_cmd;
	packet->qx_cmd = qx_cmd;
	packet->qy_cmd = qy_cmd;
	packet->qz_cmd = qz_cmd;
	packet->qo_meas = qo_meas;
	packet->qx_meas = qx_meas;
	packet->qy_meas = qy_meas;
	packet->qz_meas = qz_meas;
	packet->att_cmd = att_cmd;
	packet->throttle = throttle;

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ATT_CMD_MEASURE, (const char *)packet, MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN, MAVLINK_MSG_ID_ATT_CMD_MEASURE_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ATT_CMD_MEASURE, (const char *)packet, MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN);
#endif
#endif
}
#endif

#endif

// MESSAGE ATT_CMD_MEASURE UNPACKING


/**
 * @brief Get field att_cmd from att_cmd_measure message
 *
 * @return Control mode.  Normal flight is 1.  Motors are shut off with 0.
 */
static inline uint8_t mavlink_msg_att_cmd_measure_get_att_cmd(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint8_t(msg,  44);
}

/**
 * @brief Get field throttle from att_cmd_measure message
 *
 * @return Total thrust applied evenly to the 4 motors.
 */
static inline uint8_t mavlink_msg_att_cmd_measure_get_throttle(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint8_t(msg,  45);
}

/**
 * @brief Get field p_cmd from att_cmd_measure message
 *
 * @return Commanded roll rate.
 */
static inline float mavlink_msg_att_cmd_measure_get_p_cmd(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  0);
}

/**
 * @brief Get field q_cmd from att_cmd_measure message
 *
 * @return Commanded pitch rate.
 */
static inline float mavlink_msg_att_cmd_measure_get_q_cmd(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  4);
}

/**
 * @brief Get field r_cmd from att_cmd_measure message
 *
 * @return Commanded yaw rate.
 */
static inline float mavlink_msg_att_cmd_measure_get_r_cmd(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  8);
}

/**
 * @brief Get field qo_cmd from att_cmd_measure message
 *
 * @return Commanded quaternion 'o' value.
 */
static inline float mavlink_msg_att_cmd_measure_get_qo_cmd(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  12);
}

/**
 * @brief Get field qx_cmd from att_cmd_measure message
 *
 * @return Commanded quaternion 'x' value.
 */
static inline float mavlink_msg_att_cmd_measure_get_qx_cmd(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  16);
}

/**
 * @brief Get field qy_cmd from att_cmd_measure message
 *
 * @return Commanded quaternion 'y' value.
 */
static inline float mavlink_msg_att_cmd_measure_get_qy_cmd(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  20);
}

/**
 * @brief Get field qz_cmd from att_cmd_measure message
 *
 * @return Commanded quaternion 'z' value.
 */
static inline float mavlink_msg_att_cmd_measure_get_qz_cmd(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  24);
}

/**
 * @brief Get field qo_meas from att_cmd_measure message
 *
 * @return Measured quaternion 'o' value.
 */
static inline float mavlink_msg_att_cmd_measure_get_qo_meas(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  28);
}

/**
 * @brief Get field qx_meas from att_cmd_measure message
 *
 * @return Measured quaternion 'x' value.
 */
static inline float mavlink_msg_att_cmd_measure_get_qx_meas(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  32);
}

/**
 * @brief Get field qy_meas from att_cmd_measure message
 *
 * @return Measured quaternion 'y' value.
 */
static inline float mavlink_msg_att_cmd_measure_get_qy_meas(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  36);
}

/**
 * @brief Get field qz_meas from att_cmd_measure message
 *
 * @return Measured quaternion 'z' value.
 */
static inline float mavlink_msg_att_cmd_measure_get_qz_meas(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  40);
}

/**
 * @brief Decode a att_cmd_measure message into a struct
 *
 * @param msg The message to decode
 * @param att_cmd_measure C-struct to decode the message contents into
 */
static inline void mavlink_msg_att_cmd_measure_decode(const mavlink_message_t* msg, mavlink_att_cmd_measure_t* att_cmd_measure)
{
#if MAVLINK_NEED_BYTE_SWAP
	att_cmd_measure->p_cmd = mavlink_msg_att_cmd_measure_get_p_cmd(msg);
	att_cmd_measure->q_cmd = mavlink_msg_att_cmd_measure_get_q_cmd(msg);
	att_cmd_measure->r_cmd = mavlink_msg_att_cmd_measure_get_r_cmd(msg);
	att_cmd_measure->qo_cmd = mavlink_msg_att_cmd_measure_get_qo_cmd(msg);
	att_cmd_measure->qx_cmd = mavlink_msg_att_cmd_measure_get_qx_cmd(msg);
	att_cmd_measure->qy_cmd = mavlink_msg_att_cmd_measure_get_qy_cmd(msg);
	att_cmd_measure->qz_cmd = mavlink_msg_att_cmd_measure_get_qz_cmd(msg);
	att_cmd_measure->qo_meas = mavlink_msg_att_cmd_measure_get_qo_meas(msg);
	att_cmd_measure->qx_meas = mavlink_msg_att_cmd_measure_get_qx_meas(msg);
	att_cmd_measure->qy_meas = mavlink_msg_att_cmd_measure_get_qy_meas(msg);
	att_cmd_measure->qz_meas = mavlink_msg_att_cmd_measure_get_qz_meas(msg);
	att_cmd_measure->att_cmd = mavlink_msg_att_cmd_measure_get_att_cmd(msg);
	att_cmd_measure->throttle = mavlink_msg_att_cmd_measure_get_throttle(msg);
#else
	memcpy(att_cmd_measure, _MAV_PAYLOAD(msg), MAVLINK_MSG_ID_ATT_CMD_MEASURE_LEN);
#endif
}
