// MESSAGE GAINS PACKING

#define MAVLINK_MSG_ID_GAINS 153

typedef struct __mavlink_gains_t
{
 float Kp_roll; ///< Proportional roll gain.
 float Ki_roll; ///< Integral roll gain.
 float Kd_roll; ///< Derivative roll gain.
 float Kp_pitch; ///< Proportional pitch gain.
 float Ki_pitch; ///< Integral pitch gain.
 float Kd_pitch; ///< Derivative pitch gain.
 float Kp_yaw; ///< Proportional yaw gain.
 float Ki_yaw; ///< Integral yaw gain.
 float Kd_yaw; ///< Derivative yaw gain.
 float servo1_trim; ///< Servo 1 trim value.
 float servo2_trim; ///< Servo 2 trim value.
 float servo3_trim; ///< Servo 3 trim value.
 float servo4_trim; ///< Servo 4 trim value.
 float max_ang; ///< Maximum allowable attitude angle when flying in attitude-control mode by hand.
 float low_battery; ///< Low battery warning level in volts.
 uint8_t stream_data; ///< Stream data in real-time back to the ground station when this is true.  WARNING--xbees really can't support this during flight (two much bi-directional data).  Should be used for hand-held debugging only.
} mavlink_gains_t;

#define MAVLINK_MSG_ID_GAINS_LEN 61
#define MAVLINK_MSG_ID_153_LEN 61

#define MAVLINK_MSG_ID_GAINS_CRC 188
#define MAVLINK_MSG_ID_153_CRC 188



#define MAVLINK_MESSAGE_INFO_GAINS { \
	"GAINS", \
	16, \
	{  { "Kp_roll", NULL, MAVLINK_TYPE_FLOAT, 0, 0, offsetof(mavlink_gains_t, Kp_roll) }, \
         { "Ki_roll", NULL, MAVLINK_TYPE_FLOAT, 0, 4, offsetof(mavlink_gains_t, Ki_roll) }, \
         { "Kd_roll", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_gains_t, Kd_roll) }, \
         { "Kp_pitch", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_gains_t, Kp_pitch) }, \
         { "Ki_pitch", NULL, MAVLINK_TYPE_FLOAT, 0, 16, offsetof(mavlink_gains_t, Ki_pitch) }, \
         { "Kd_pitch", NULL, MAVLINK_TYPE_FLOAT, 0, 20, offsetof(mavlink_gains_t, Kd_pitch) }, \
         { "Kp_yaw", NULL, MAVLINK_TYPE_FLOAT, 0, 24, offsetof(mavlink_gains_t, Kp_yaw) }, \
         { "Ki_yaw", NULL, MAVLINK_TYPE_FLOAT, 0, 28, offsetof(mavlink_gains_t, Ki_yaw) }, \
         { "Kd_yaw", NULL, MAVLINK_TYPE_FLOAT, 0, 32, offsetof(mavlink_gains_t, Kd_yaw) }, \
         { "servo1_trim", NULL, MAVLINK_TYPE_FLOAT, 0, 36, offsetof(mavlink_gains_t, servo1_trim) }, \
         { "servo2_trim", NULL, MAVLINK_TYPE_FLOAT, 0, 40, offsetof(mavlink_gains_t, servo2_trim) }, \
         { "servo3_trim", NULL, MAVLINK_TYPE_FLOAT, 0, 44, offsetof(mavlink_gains_t, servo3_trim) }, \
         { "servo4_trim", NULL, MAVLINK_TYPE_FLOAT, 0, 48, offsetof(mavlink_gains_t, servo4_trim) }, \
         { "max_ang", NULL, MAVLINK_TYPE_FLOAT, 0, 52, offsetof(mavlink_gains_t, max_ang) }, \
         { "low_battery", NULL, MAVLINK_TYPE_FLOAT, 0, 56, offsetof(mavlink_gains_t, low_battery) }, \
         { "stream_data", NULL, MAVLINK_TYPE_UINT8_T, 0, 60, offsetof(mavlink_gains_t, stream_data) }, \
         } \
}


/**
 * @brief Pack a gains message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param Kp_roll Proportional roll gain.
 * @param Ki_roll Integral roll gain.
 * @param Kd_roll Derivative roll gain.
 * @param Kp_pitch Proportional pitch gain.
 * @param Ki_pitch Integral pitch gain.
 * @param Kd_pitch Derivative pitch gain.
 * @param Kp_yaw Proportional yaw gain.
 * @param Ki_yaw Integral yaw gain.
 * @param Kd_yaw Derivative yaw gain.
 * @param servo1_trim Servo 1 trim value.
 * @param servo2_trim Servo 2 trim value.
 * @param servo3_trim Servo 3 trim value.
 * @param servo4_trim Servo 4 trim value.
 * @param max_ang Maximum allowable attitude angle when flying in attitude-control mode by hand.
 * @param low_battery Low battery warning level in volts.
 * @param stream_data Stream data in real-time back to the ground station when this is true.  WARNING--xbees really can't support this during flight (two much bi-directional data).  Should be used for hand-held debugging only.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_gains_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
						       float Kp_roll, float Ki_roll, float Kd_roll, float Kp_pitch, float Ki_pitch, float Kd_pitch, float Kp_yaw, float Ki_yaw, float Kd_yaw, float servo1_trim, float servo2_trim, float servo3_trim, float servo4_trim, float max_ang, float low_battery, uint8_t stream_data)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_GAINS_LEN];
	_mav_put_float(buf, 0, Kp_roll);
	_mav_put_float(buf, 4, Ki_roll);
	_mav_put_float(buf, 8, Kd_roll);
	_mav_put_float(buf, 12, Kp_pitch);
	_mav_put_float(buf, 16, Ki_pitch);
	_mav_put_float(buf, 20, Kd_pitch);
	_mav_put_float(buf, 24, Kp_yaw);
	_mav_put_float(buf, 28, Ki_yaw);
	_mav_put_float(buf, 32, Kd_yaw);
	_mav_put_float(buf, 36, servo1_trim);
	_mav_put_float(buf, 40, servo2_trim);
	_mav_put_float(buf, 44, servo3_trim);
	_mav_put_float(buf, 48, servo4_trim);
	_mav_put_float(buf, 52, max_ang);
	_mav_put_float(buf, 56, low_battery);
	_mav_put_uint8_t(buf, 60, stream_data);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_GAINS_LEN);
#else
	mavlink_gains_t packet;
	packet.Kp_roll = Kp_roll;
	packet.Ki_roll = Ki_roll;
	packet.Kd_roll = Kd_roll;
	packet.Kp_pitch = Kp_pitch;
	packet.Ki_pitch = Ki_pitch;
	packet.Kd_pitch = Kd_pitch;
	packet.Kp_yaw = Kp_yaw;
	packet.Ki_yaw = Ki_yaw;
	packet.Kd_yaw = Kd_yaw;
	packet.servo1_trim = servo1_trim;
	packet.servo2_trim = servo2_trim;
	packet.servo3_trim = servo3_trim;
	packet.servo4_trim = servo4_trim;
	packet.max_ang = max_ang;
	packet.low_battery = low_battery;
	packet.stream_data = stream_data;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_GAINS_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_GAINS;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_GAINS_LEN, MAVLINK_MSG_ID_GAINS_CRC);
#else
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_GAINS_LEN);
#endif
}

/**
 * @brief Pack a gains message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param Kp_roll Proportional roll gain.
 * @param Ki_roll Integral roll gain.
 * @param Kd_roll Derivative roll gain.
 * @param Kp_pitch Proportional pitch gain.
 * @param Ki_pitch Integral pitch gain.
 * @param Kd_pitch Derivative pitch gain.
 * @param Kp_yaw Proportional yaw gain.
 * @param Ki_yaw Integral yaw gain.
 * @param Kd_yaw Derivative yaw gain.
 * @param servo1_trim Servo 1 trim value.
 * @param servo2_trim Servo 2 trim value.
 * @param servo3_trim Servo 3 trim value.
 * @param servo4_trim Servo 4 trim value.
 * @param max_ang Maximum allowable attitude angle when flying in attitude-control mode by hand.
 * @param low_battery Low battery warning level in volts.
 * @param stream_data Stream data in real-time back to the ground station when this is true.  WARNING--xbees really can't support this during flight (two much bi-directional data).  Should be used for hand-held debugging only.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_gains_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
							   mavlink_message_t* msg,
						           float Kp_roll,float Ki_roll,float Kd_roll,float Kp_pitch,float Ki_pitch,float Kd_pitch,float Kp_yaw,float Ki_yaw,float Kd_yaw,float servo1_trim,float servo2_trim,float servo3_trim,float servo4_trim,float max_ang,float low_battery,uint8_t stream_data)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_GAINS_LEN];
	_mav_put_float(buf, 0, Kp_roll);
	_mav_put_float(buf, 4, Ki_roll);
	_mav_put_float(buf, 8, Kd_roll);
	_mav_put_float(buf, 12, Kp_pitch);
	_mav_put_float(buf, 16, Ki_pitch);
	_mav_put_float(buf, 20, Kd_pitch);
	_mav_put_float(buf, 24, Kp_yaw);
	_mav_put_float(buf, 28, Ki_yaw);
	_mav_put_float(buf, 32, Kd_yaw);
	_mav_put_float(buf, 36, servo1_trim);
	_mav_put_float(buf, 40, servo2_trim);
	_mav_put_float(buf, 44, servo3_trim);
	_mav_put_float(buf, 48, servo4_trim);
	_mav_put_float(buf, 52, max_ang);
	_mav_put_float(buf, 56, low_battery);
	_mav_put_uint8_t(buf, 60, stream_data);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_GAINS_LEN);
#else
	mavlink_gains_t packet;
	packet.Kp_roll = Kp_roll;
	packet.Ki_roll = Ki_roll;
	packet.Kd_roll = Kd_roll;
	packet.Kp_pitch = Kp_pitch;
	packet.Ki_pitch = Ki_pitch;
	packet.Kd_pitch = Kd_pitch;
	packet.Kp_yaw = Kp_yaw;
	packet.Ki_yaw = Ki_yaw;
	packet.Kd_yaw = Kd_yaw;
	packet.servo1_trim = servo1_trim;
	packet.servo2_trim = servo2_trim;
	packet.servo3_trim = servo3_trim;
	packet.servo4_trim = servo4_trim;
	packet.max_ang = max_ang;
	packet.low_battery = low_battery;
	packet.stream_data = stream_data;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_GAINS_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_GAINS;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_GAINS_LEN, MAVLINK_MSG_ID_GAINS_CRC);
#else
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_GAINS_LEN);
#endif
}

/**
 * @brief Encode a gains struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param gains C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_gains_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_gains_t* gains)
{
	return mavlink_msg_gains_pack(system_id, component_id, msg, gains->Kp_roll, gains->Ki_roll, gains->Kd_roll, gains->Kp_pitch, gains->Ki_pitch, gains->Kd_pitch, gains->Kp_yaw, gains->Ki_yaw, gains->Kd_yaw, gains->servo1_trim, gains->servo2_trim, gains->servo3_trim, gains->servo4_trim, gains->max_ang, gains->low_battery, gains->stream_data);
}

/**
 * @brief Encode a gains struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param gains C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_gains_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_gains_t* gains)
{
	return mavlink_msg_gains_pack_chan(system_id, component_id, chan, msg, gains->Kp_roll, gains->Ki_roll, gains->Kd_roll, gains->Kp_pitch, gains->Ki_pitch, gains->Kd_pitch, gains->Kp_yaw, gains->Ki_yaw, gains->Kd_yaw, gains->servo1_trim, gains->servo2_trim, gains->servo3_trim, gains->servo4_trim, gains->max_ang, gains->low_battery, gains->stream_data);
}

/**
 * @brief Send a gains message
 * @param chan MAVLink channel to send the message
 *
 * @param Kp_roll Proportional roll gain.
 * @param Ki_roll Integral roll gain.
 * @param Kd_roll Derivative roll gain.
 * @param Kp_pitch Proportional pitch gain.
 * @param Ki_pitch Integral pitch gain.
 * @param Kd_pitch Derivative pitch gain.
 * @param Kp_yaw Proportional yaw gain.
 * @param Ki_yaw Integral yaw gain.
 * @param Kd_yaw Derivative yaw gain.
 * @param servo1_trim Servo 1 trim value.
 * @param servo2_trim Servo 2 trim value.
 * @param servo3_trim Servo 3 trim value.
 * @param servo4_trim Servo 4 trim value.
 * @param max_ang Maximum allowable attitude angle when flying in attitude-control mode by hand.
 * @param low_battery Low battery warning level in volts.
 * @param stream_data Stream data in real-time back to the ground station when this is true.  WARNING--xbees really can't support this during flight (two much bi-directional data).  Should be used for hand-held debugging only.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_gains_send(mavlink_channel_t chan, float Kp_roll, float Ki_roll, float Kd_roll, float Kp_pitch, float Ki_pitch, float Kd_pitch, float Kp_yaw, float Ki_yaw, float Kd_yaw, float servo1_trim, float servo2_trim, float servo3_trim, float servo4_trim, float max_ang, float low_battery, uint8_t stream_data)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_GAINS_LEN];
	_mav_put_float(buf, 0, Kp_roll);
	_mav_put_float(buf, 4, Ki_roll);
	_mav_put_float(buf, 8, Kd_roll);
	_mav_put_float(buf, 12, Kp_pitch);
	_mav_put_float(buf, 16, Ki_pitch);
	_mav_put_float(buf, 20, Kd_pitch);
	_mav_put_float(buf, 24, Kp_yaw);
	_mav_put_float(buf, 28, Ki_yaw);
	_mav_put_float(buf, 32, Kd_yaw);
	_mav_put_float(buf, 36, servo1_trim);
	_mav_put_float(buf, 40, servo2_trim);
	_mav_put_float(buf, 44, servo3_trim);
	_mav_put_float(buf, 48, servo4_trim);
	_mav_put_float(buf, 52, max_ang);
	_mav_put_float(buf, 56, low_battery);
	_mav_put_uint8_t(buf, 60, stream_data);

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_GAINS, buf, MAVLINK_MSG_ID_GAINS_LEN, MAVLINK_MSG_ID_GAINS_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_GAINS, buf, MAVLINK_MSG_ID_GAINS_LEN);
#endif
#else
	mavlink_gains_t packet;
	packet.Kp_roll = Kp_roll;
	packet.Ki_roll = Ki_roll;
	packet.Kd_roll = Kd_roll;
	packet.Kp_pitch = Kp_pitch;
	packet.Ki_pitch = Ki_pitch;
	packet.Kd_pitch = Kd_pitch;
	packet.Kp_yaw = Kp_yaw;
	packet.Ki_yaw = Ki_yaw;
	packet.Kd_yaw = Kd_yaw;
	packet.servo1_trim = servo1_trim;
	packet.servo2_trim = servo2_trim;
	packet.servo3_trim = servo3_trim;
	packet.servo4_trim = servo4_trim;
	packet.max_ang = max_ang;
	packet.low_battery = low_battery;
	packet.stream_data = stream_data;

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_GAINS, (const char *)&packet, MAVLINK_MSG_ID_GAINS_LEN, MAVLINK_MSG_ID_GAINS_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_GAINS, (const char *)&packet, MAVLINK_MSG_ID_GAINS_LEN);
#endif
#endif
}

#if MAVLINK_MSG_ID_GAINS_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_gains_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  float Kp_roll, float Ki_roll, float Kd_roll, float Kp_pitch, float Ki_pitch, float Kd_pitch, float Kp_yaw, float Ki_yaw, float Kd_yaw, float servo1_trim, float servo2_trim, float servo3_trim, float servo4_trim, float max_ang, float low_battery, uint8_t stream_data)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char *buf = (char *)msgbuf;
	_mav_put_float(buf, 0, Kp_roll);
	_mav_put_float(buf, 4, Ki_roll);
	_mav_put_float(buf, 8, Kd_roll);
	_mav_put_float(buf, 12, Kp_pitch);
	_mav_put_float(buf, 16, Ki_pitch);
	_mav_put_float(buf, 20, Kd_pitch);
	_mav_put_float(buf, 24, Kp_yaw);
	_mav_put_float(buf, 28, Ki_yaw);
	_mav_put_float(buf, 32, Kd_yaw);
	_mav_put_float(buf, 36, servo1_trim);
	_mav_put_float(buf, 40, servo2_trim);
	_mav_put_float(buf, 44, servo3_trim);
	_mav_put_float(buf, 48, servo4_trim);
	_mav_put_float(buf, 52, max_ang);
	_mav_put_float(buf, 56, low_battery);
	_mav_put_uint8_t(buf, 60, stream_data);

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_GAINS, buf, MAVLINK_MSG_ID_GAINS_LEN, MAVLINK_MSG_ID_GAINS_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_GAINS, buf, MAVLINK_MSG_ID_GAINS_LEN);
#endif
#else
	mavlink_gains_t *packet = (mavlink_gains_t *)msgbuf;
	packet->Kp_roll = Kp_roll;
	packet->Ki_roll = Ki_roll;
	packet->Kd_roll = Kd_roll;
	packet->Kp_pitch = Kp_pitch;
	packet->Ki_pitch = Ki_pitch;
	packet->Kd_pitch = Kd_pitch;
	packet->Kp_yaw = Kp_yaw;
	packet->Ki_yaw = Ki_yaw;
	packet->Kd_yaw = Kd_yaw;
	packet->servo1_trim = servo1_trim;
	packet->servo2_trim = servo2_trim;
	packet->servo3_trim = servo3_trim;
	packet->servo4_trim = servo4_trim;
	packet->max_ang = max_ang;
	packet->low_battery = low_battery;
	packet->stream_data = stream_data;

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_GAINS, (const char *)packet, MAVLINK_MSG_ID_GAINS_LEN, MAVLINK_MSG_ID_GAINS_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_GAINS, (const char *)packet, MAVLINK_MSG_ID_GAINS_LEN);
#endif
#endif
}
#endif

#endif

// MESSAGE GAINS UNPACKING


/**
 * @brief Get field Kp_roll from gains message
 *
 * @return Proportional roll gain.
 */
static inline float mavlink_msg_gains_get_Kp_roll(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  0);
}

/**
 * @brief Get field Ki_roll from gains message
 *
 * @return Integral roll gain.
 */
static inline float mavlink_msg_gains_get_Ki_roll(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  4);
}

/**
 * @brief Get field Kd_roll from gains message
 *
 * @return Derivative roll gain.
 */
static inline float mavlink_msg_gains_get_Kd_roll(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  8);
}

/**
 * @brief Get field Kp_pitch from gains message
 *
 * @return Proportional pitch gain.
 */
static inline float mavlink_msg_gains_get_Kp_pitch(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  12);
}

/**
 * @brief Get field Ki_pitch from gains message
 *
 * @return Integral pitch gain.
 */
static inline float mavlink_msg_gains_get_Ki_pitch(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  16);
}

/**
 * @brief Get field Kd_pitch from gains message
 *
 * @return Derivative pitch gain.
 */
static inline float mavlink_msg_gains_get_Kd_pitch(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  20);
}

/**
 * @brief Get field Kp_yaw from gains message
 *
 * @return Proportional yaw gain.
 */
static inline float mavlink_msg_gains_get_Kp_yaw(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  24);
}

/**
 * @brief Get field Ki_yaw from gains message
 *
 * @return Integral yaw gain.
 */
static inline float mavlink_msg_gains_get_Ki_yaw(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  28);
}

/**
 * @brief Get field Kd_yaw from gains message
 *
 * @return Derivative yaw gain.
 */
static inline float mavlink_msg_gains_get_Kd_yaw(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  32);
}

/**
 * @brief Get field servo1_trim from gains message
 *
 * @return Servo 1 trim value.
 */
static inline float mavlink_msg_gains_get_servo1_trim(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  36);
}

/**
 * @brief Get field servo2_trim from gains message
 *
 * @return Servo 2 trim value.
 */
static inline float mavlink_msg_gains_get_servo2_trim(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  40);
}

/**
 * @brief Get field servo3_trim from gains message
 *
 * @return Servo 3 trim value.
 */
static inline float mavlink_msg_gains_get_servo3_trim(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  44);
}

/**
 * @brief Get field servo4_trim from gains message
 *
 * @return Servo 4 trim value.
 */
static inline float mavlink_msg_gains_get_servo4_trim(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  48);
}

/**
 * @brief Get field max_ang from gains message
 *
 * @return Maximum allowable attitude angle when flying in attitude-control mode by hand.
 */
static inline float mavlink_msg_gains_get_max_ang(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  52);
}

/**
 * @brief Get field low_battery from gains message
 *
 * @return Low battery warning level in volts.
 */
static inline float mavlink_msg_gains_get_low_battery(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  56);
}

/**
 * @brief Get field stream_data from gains message
 *
 * @return Stream data in real-time back to the ground station when this is true.  WARNING--xbees really can't support this during flight (two much bi-directional data).  Should be used for hand-held debugging only.
 */
static inline uint8_t mavlink_msg_gains_get_stream_data(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint8_t(msg,  60);
}

/**
 * @brief Decode a gains message into a struct
 *
 * @param msg The message to decode
 * @param gains C-struct to decode the message contents into
 */
static inline void mavlink_msg_gains_decode(const mavlink_message_t* msg, mavlink_gains_t* gains)
{
#if MAVLINK_NEED_BYTE_SWAP
	gains->Kp_roll = mavlink_msg_gains_get_Kp_roll(msg);
	gains->Ki_roll = mavlink_msg_gains_get_Ki_roll(msg);
	gains->Kd_roll = mavlink_msg_gains_get_Kd_roll(msg);
	gains->Kp_pitch = mavlink_msg_gains_get_Kp_pitch(msg);
	gains->Ki_pitch = mavlink_msg_gains_get_Ki_pitch(msg);
	gains->Kd_pitch = mavlink_msg_gains_get_Kd_pitch(msg);
	gains->Kp_yaw = mavlink_msg_gains_get_Kp_yaw(msg);
	gains->Ki_yaw = mavlink_msg_gains_get_Ki_yaw(msg);
	gains->Kd_yaw = mavlink_msg_gains_get_Kd_yaw(msg);
	gains->servo1_trim = mavlink_msg_gains_get_servo1_trim(msg);
	gains->servo2_trim = mavlink_msg_gains_get_servo2_trim(msg);
	gains->servo3_trim = mavlink_msg_gains_get_servo3_trim(msg);
	gains->servo4_trim = mavlink_msg_gains_get_servo4_trim(msg);
	gains->max_ang = mavlink_msg_gains_get_max_ang(msg);
	gains->low_battery = mavlink_msg_gains_get_low_battery(msg);
	gains->stream_data = mavlink_msg_gains_get_stream_data(msg);
#else
	memcpy(gains, _MAV_PAYLOAD(msg), MAVLINK_MSG_ID_GAINS_LEN);
#endif
}
