/** @file
 *	@brief MAVLink comm protocol testsuite generated from acl_msgs.xml
 *	@see http://qgroundcontrol.org/mavlink/
 */
#ifndef ACL_MSGS_TESTSUITE_H
#define ACL_MSGS_TESTSUITE_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef MAVLINK_TEST_ALL
#define MAVLINK_TEST_ALL

static void mavlink_test_acl_msgs(uint8_t, uint8_t, mavlink_message_t *last_msg);

static void mavlink_test_all(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{

	mavlink_test_acl_msgs(system_id, component_id, last_msg);
}
#endif




static void mavlink_test_att_cmd_measure(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_att_cmd_measure_t packet_in = {
		17.0,45.0,73.0,101.0,129.0,157.0,185.0,213.0,241.0,269.0,297.0,137,204
    };
	mavlink_att_cmd_measure_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.p_cmd = packet_in.p_cmd;
        	packet1.q_cmd = packet_in.q_cmd;
        	packet1.r_cmd = packet_in.r_cmd;
        	packet1.qo_cmd = packet_in.qo_cmd;
        	packet1.qx_cmd = packet_in.qx_cmd;
        	packet1.qy_cmd = packet_in.qy_cmd;
        	packet1.qz_cmd = packet_in.qz_cmd;
        	packet1.qo_meas = packet_in.qo_meas;
        	packet1.qx_meas = packet_in.qx_meas;
        	packet1.qy_meas = packet_in.qy_meas;
        	packet1.qz_meas = packet_in.qz_meas;
        	packet1.att_cmd = packet_in.att_cmd;
        	packet1.throttle = packet_in.throttle;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_att_cmd_measure_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_att_cmd_measure_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_att_cmd_measure_pack(system_id, component_id, &msg , packet1.att_cmd , packet1.throttle , packet1.p_cmd , packet1.q_cmd , packet1.r_cmd , packet1.qo_cmd , packet1.qx_cmd , packet1.qy_cmd , packet1.qz_cmd , packet1.qo_meas , packet1.qx_meas , packet1.qy_meas , packet1.qz_meas );
	mavlink_msg_att_cmd_measure_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_att_cmd_measure_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.att_cmd , packet1.throttle , packet1.p_cmd , packet1.q_cmd , packet1.r_cmd , packet1.qo_cmd , packet1.qx_cmd , packet1.qy_cmd , packet1.qz_cmd , packet1.qo_meas , packet1.qx_meas , packet1.qy_meas , packet1.qz_meas );
	mavlink_msg_att_cmd_measure_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_att_cmd_measure_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_att_cmd_measure_send(MAVLINK_COMM_1 , packet1.att_cmd , packet1.throttle , packet1.p_cmd , packet1.q_cmd , packet1.r_cmd , packet1.qo_cmd , packet1.qx_cmd , packet1.qy_cmd , packet1.qz_cmd , packet1.qo_meas , packet1.qx_meas , packet1.qy_meas , packet1.qz_meas );
	mavlink_msg_att_cmd_measure_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_battery_voltage(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_battery_voltage_t packet_in = {
		963497464,17443
    };
	mavlink_battery_voltage_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.time_boot_ms = packet_in.time_boot_ms;
        	packet1.mvolts = packet_in.mvolts;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_battery_voltage_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_battery_voltage_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_battery_voltage_pack(system_id, component_id, &msg , packet1.time_boot_ms , packet1.mvolts );
	mavlink_msg_battery_voltage_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_battery_voltage_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.time_boot_ms , packet1.mvolts );
	mavlink_msg_battery_voltage_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_battery_voltage_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_battery_voltage_send(MAVLINK_COMM_1 , packet1.time_boot_ms , packet1.mvolts );
	mavlink_msg_battery_voltage_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_sensor_cal(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_sensor_cal_t packet_in = {
		17.0,45.0,73.0,101.0
    };
	mavlink_sensor_cal_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.gyro_scale = packet_in.gyro_scale;
        	packet1.accel_scale = packet_in.accel_scale;
        	packet1.k_att_filter = packet_in.k_att_filter;
        	packet1.k_gyro_bias = packet_in.k_gyro_bias;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_sensor_cal_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_sensor_cal_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_sensor_cal_pack(system_id, component_id, &msg , packet1.gyro_scale , packet1.accel_scale , packet1.k_att_filter , packet1.k_gyro_bias );
	mavlink_msg_sensor_cal_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_sensor_cal_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.gyro_scale , packet1.accel_scale , packet1.k_att_filter , packet1.k_gyro_bias );
	mavlink_msg_sensor_cal_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_sensor_cal_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_sensor_cal_send(MAVLINK_COMM_1 , packet1.gyro_scale , packet1.accel_scale , packet1.k_att_filter , packet1.k_gyro_bias );
	mavlink_msg_sensor_cal_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_gains(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_gains_t packet_in = {
		17.0,45.0,73.0,101.0,129.0,157.0,185.0,213.0,241.0,269.0,297.0,325.0,353.0,381.0,409.0,185
    };
	mavlink_gains_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.Kp_roll = packet_in.Kp_roll;
        	packet1.Ki_roll = packet_in.Ki_roll;
        	packet1.Kd_roll = packet_in.Kd_roll;
        	packet1.Kp_pitch = packet_in.Kp_pitch;
        	packet1.Ki_pitch = packet_in.Ki_pitch;
        	packet1.Kd_pitch = packet_in.Kd_pitch;
        	packet1.Kp_yaw = packet_in.Kp_yaw;
        	packet1.Ki_yaw = packet_in.Ki_yaw;
        	packet1.Kd_yaw = packet_in.Kd_yaw;
        	packet1.servo1_trim = packet_in.servo1_trim;
        	packet1.servo2_trim = packet_in.servo2_trim;
        	packet1.servo3_trim = packet_in.servo3_trim;
        	packet1.servo4_trim = packet_in.servo4_trim;
        	packet1.max_ang = packet_in.max_ang;
        	packet1.low_battery = packet_in.low_battery;
        	packet1.stream_data = packet_in.stream_data;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_gains_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_gains_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_gains_pack(system_id, component_id, &msg , packet1.Kp_roll , packet1.Ki_roll , packet1.Kd_roll , packet1.Kp_pitch , packet1.Ki_pitch , packet1.Kd_pitch , packet1.Kp_yaw , packet1.Ki_yaw , packet1.Kd_yaw , packet1.servo1_trim , packet1.servo2_trim , packet1.servo3_trim , packet1.servo4_trim , packet1.max_ang , packet1.low_battery , packet1.stream_data );
	mavlink_msg_gains_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_gains_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.Kp_roll , packet1.Ki_roll , packet1.Kd_roll , packet1.Kp_pitch , packet1.Ki_pitch , packet1.Kd_pitch , packet1.Kp_yaw , packet1.Ki_yaw , packet1.Kd_yaw , packet1.servo1_trim , packet1.servo2_trim , packet1.servo3_trim , packet1.servo4_trim , packet1.max_ang , packet1.low_battery , packet1.stream_data );
	mavlink_msg_gains_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_gains_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_gains_send(MAVLINK_COMM_1 , packet1.Kp_roll , packet1.Ki_roll , packet1.Kd_roll , packet1.Kp_pitch , packet1.Ki_pitch , packet1.Kd_pitch , packet1.Kp_yaw , packet1.Ki_yaw , packet1.Kd_yaw , packet1.servo1_trim , packet1.servo2_trim , packet1.servo3_trim , packet1.servo4_trim , packet1.max_ang , packet1.low_battery , packet1.stream_data );
	mavlink_msg_gains_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_sensors(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_sensors_t packet_in = {
		17.0,45.0,73.0,101.0,129.0,157.0,185.0,213.0,241.0,269.0,297.0
    };
	mavlink_sensors_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.gyro_x = packet_in.gyro_x;
        	packet1.gyro_y = packet_in.gyro_y;
        	packet1.gyro_z = packet_in.gyro_z;
        	packet1.accel_x = packet_in.accel_x;
        	packet1.accel_y = packet_in.accel_y;
        	packet1.accel_z = packet_in.accel_z;
        	packet1.mag_x = packet_in.mag_x;
        	packet1.mag_y = packet_in.mag_y;
        	packet1.mag_z = packet_in.mag_z;
        	packet1.pressure = packet_in.pressure;
        	packet1.temperature = packet_in.temperature;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_sensors_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_sensors_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_sensors_pack(system_id, component_id, &msg , packet1.gyro_x , packet1.gyro_y , packet1.gyro_z , packet1.accel_x , packet1.accel_y , packet1.accel_z , packet1.mag_x , packet1.mag_y , packet1.mag_z , packet1.pressure , packet1.temperature );
	mavlink_msg_sensors_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_sensors_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.gyro_x , packet1.gyro_y , packet1.gyro_z , packet1.accel_x , packet1.accel_y , packet1.accel_z , packet1.mag_x , packet1.mag_y , packet1.mag_z , packet1.pressure , packet1.temperature );
	mavlink_msg_sensors_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_sensors_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_sensors_send(MAVLINK_COMM_1 , packet1.gyro_x , packet1.gyro_y , packet1.gyro_z , packet1.accel_x , packet1.accel_y , packet1.accel_z , packet1.mag_x , packet1.mag_y , packet1.mag_z , packet1.pressure , packet1.temperature );
	mavlink_msg_sensors_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_latency_test(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_latency_test_t packet_in = {
		123.0,93372036854776311LL,963498296,18275
    };
	mavlink_latency_test_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.send_time = packet_in.send_time;
        	packet1.fill1 = packet_in.fill1;
        	packet1.fill2 = packet_in.fill2;
        	packet1.fill3 = packet_in.fill3;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_latency_test_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_latency_test_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_latency_test_pack(system_id, component_id, &msg , packet1.send_time , packet1.fill1 , packet1.fill2 , packet1.fill3 );
	mavlink_msg_latency_test_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_latency_test_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.send_time , packet1.fill1 , packet1.fill2 , packet1.fill3 );
	mavlink_msg_latency_test_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_latency_test_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_latency_test_send(MAVLINK_COMM_1 , packet1.send_time , packet1.fill1 , packet1.fill2 , packet1.fill3 );
	mavlink_msg_latency_test_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_acl_msgs(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_test_att_cmd_measure(system_id, component_id, last_msg);
	mavlink_test_battery_voltage(system_id, component_id, last_msg);
	mavlink_test_sensor_cal(system_id, component_id, last_msg);
	mavlink_test_gains(system_id, component_id, last_msg);
	mavlink_test_sensors(system_id, component_id, last_msg);
	mavlink_test_latency_test(system_id, component_id, last_msg);
}

#ifdef __cplusplus
}
#endif // __cplusplus
#endif // ACL_MSGS_TESTSUITE_H
