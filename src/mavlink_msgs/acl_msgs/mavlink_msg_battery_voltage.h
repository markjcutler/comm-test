// MESSAGE BATTERY_VOLTAGE PACKING

#define MAVLINK_MSG_ID_BATTERY_VOLTAGE 151

typedef struct __mavlink_battery_voltage_t
{
 uint32_t time_boot_ms; ///< Timestamp in milliseconds since system boot.
 int16_t mvolts; ///< Voltage in millivolts.
} mavlink_battery_voltage_t;

#define MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN 6
#define MAVLINK_MSG_ID_151_LEN 6

#define MAVLINK_MSG_ID_BATTERY_VOLTAGE_CRC 171
#define MAVLINK_MSG_ID_151_CRC 171



#define MAVLINK_MESSAGE_INFO_BATTERY_VOLTAGE { \
	"BATTERY_VOLTAGE", \
	2, \
	{  { "time_boot_ms", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_battery_voltage_t, time_boot_ms) }, \
         { "mvolts", NULL, MAVLINK_TYPE_INT16_T, 0, 4, offsetof(mavlink_battery_voltage_t, mvolts) }, \
         } \
}


/**
 * @brief Pack a battery_voltage message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time_boot_ms Timestamp in milliseconds since system boot.
 * @param mvolts Voltage in millivolts.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_battery_voltage_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
						       uint32_t time_boot_ms, int16_t mvolts)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN];
	_mav_put_uint32_t(buf, 0, time_boot_ms);
	_mav_put_int16_t(buf, 4, mvolts);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN);
#else
	mavlink_battery_voltage_t packet;
	packet.time_boot_ms = time_boot_ms;
	packet.mvolts = mvolts;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_BATTERY_VOLTAGE;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN, MAVLINK_MSG_ID_BATTERY_VOLTAGE_CRC);
#else
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN);
#endif
}

/**
 * @brief Pack a battery_voltage message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time_boot_ms Timestamp in milliseconds since system boot.
 * @param mvolts Voltage in millivolts.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_battery_voltage_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
							   mavlink_message_t* msg,
						           uint32_t time_boot_ms,int16_t mvolts)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN];
	_mav_put_uint32_t(buf, 0, time_boot_ms);
	_mav_put_int16_t(buf, 4, mvolts);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN);
#else
	mavlink_battery_voltage_t packet;
	packet.time_boot_ms = time_boot_ms;
	packet.mvolts = mvolts;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_BATTERY_VOLTAGE;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN, MAVLINK_MSG_ID_BATTERY_VOLTAGE_CRC);
#else
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN);
#endif
}

/**
 * @brief Encode a battery_voltage struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param battery_voltage C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_battery_voltage_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_battery_voltage_t* battery_voltage)
{
	return mavlink_msg_battery_voltage_pack(system_id, component_id, msg, battery_voltage->time_boot_ms, battery_voltage->mvolts);
}

/**
 * @brief Encode a battery_voltage struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param battery_voltage C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_battery_voltage_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_battery_voltage_t* battery_voltage)
{
	return mavlink_msg_battery_voltage_pack_chan(system_id, component_id, chan, msg, battery_voltage->time_boot_ms, battery_voltage->mvolts);
}

/**
 * @brief Send a battery_voltage message
 * @param chan MAVLink channel to send the message
 *
 * @param time_boot_ms Timestamp in milliseconds since system boot.
 * @param mvolts Voltage in millivolts.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_battery_voltage_send(mavlink_channel_t chan, uint32_t time_boot_ms, int16_t mvolts)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN];
	_mav_put_uint32_t(buf, 0, time_boot_ms);
	_mav_put_int16_t(buf, 4, mvolts);

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_BATTERY_VOLTAGE, buf, MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN, MAVLINK_MSG_ID_BATTERY_VOLTAGE_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_BATTERY_VOLTAGE, buf, MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN);
#endif
#else
	mavlink_battery_voltage_t packet;
	packet.time_boot_ms = time_boot_ms;
	packet.mvolts = mvolts;

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_BATTERY_VOLTAGE, (const char *)&packet, MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN, MAVLINK_MSG_ID_BATTERY_VOLTAGE_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_BATTERY_VOLTAGE, (const char *)&packet, MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN);
#endif
#endif
}

#if MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_battery_voltage_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time_boot_ms, int16_t mvolts)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char *buf = (char *)msgbuf;
	_mav_put_uint32_t(buf, 0, time_boot_ms);
	_mav_put_int16_t(buf, 4, mvolts);

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_BATTERY_VOLTAGE, buf, MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN, MAVLINK_MSG_ID_BATTERY_VOLTAGE_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_BATTERY_VOLTAGE, buf, MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN);
#endif
#else
	mavlink_battery_voltage_t *packet = (mavlink_battery_voltage_t *)msgbuf;
	packet->time_boot_ms = time_boot_ms;
	packet->mvolts = mvolts;

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_BATTERY_VOLTAGE, (const char *)packet, MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN, MAVLINK_MSG_ID_BATTERY_VOLTAGE_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_BATTERY_VOLTAGE, (const char *)packet, MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN);
#endif
#endif
}
#endif

#endif

// MESSAGE BATTERY_VOLTAGE UNPACKING


/**
 * @brief Get field time_boot_ms from battery_voltage message
 *
 * @return Timestamp in milliseconds since system boot.
 */
static inline uint32_t mavlink_msg_battery_voltage_get_time_boot_ms(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field mvolts from battery_voltage message
 *
 * @return Voltage in millivolts.
 */
static inline int16_t mavlink_msg_battery_voltage_get_mvolts(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int16_t(msg,  4);
}

/**
 * @brief Decode a battery_voltage message into a struct
 *
 * @param msg The message to decode
 * @param battery_voltage C-struct to decode the message contents into
 */
static inline void mavlink_msg_battery_voltage_decode(const mavlink_message_t* msg, mavlink_battery_voltage_t* battery_voltage)
{
#if MAVLINK_NEED_BYTE_SWAP
	battery_voltage->time_boot_ms = mavlink_msg_battery_voltage_get_time_boot_ms(msg);
	battery_voltage->mvolts = mavlink_msg_battery_voltage_get_mvolts(msg);
#else
	memcpy(battery_voltage, _MAV_PAYLOAD(msg), MAVLINK_MSG_ID_BATTERY_VOLTAGE_LEN);
#endif
}
