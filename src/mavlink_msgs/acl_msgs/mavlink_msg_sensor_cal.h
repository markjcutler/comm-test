// MESSAGE SENSOR_CAL PACKING

#define MAVLINK_MSG_ID_SENSOR_CAL 152

typedef struct __mavlink_sensor_cal_t
{
 float gyro_scale; ///< Gyro scale.
 float accel_scale; ///< Accelerometer scale.
 float k_att_filter; ///< Gain on the complimentary attitude filter.
 float k_gyro_bias; ///< Gain on the gyro bias estimator.
} mavlink_sensor_cal_t;

#define MAVLINK_MSG_ID_SENSOR_CAL_LEN 16
#define MAVLINK_MSG_ID_152_LEN 16

#define MAVLINK_MSG_ID_SENSOR_CAL_CRC 168
#define MAVLINK_MSG_ID_152_CRC 168



#define MAVLINK_MESSAGE_INFO_SENSOR_CAL { \
	"SENSOR_CAL", \
	4, \
	{  { "gyro_scale", NULL, MAVLINK_TYPE_FLOAT, 0, 0, offsetof(mavlink_sensor_cal_t, gyro_scale) }, \
         { "accel_scale", NULL, MAVLINK_TYPE_FLOAT, 0, 4, offsetof(mavlink_sensor_cal_t, accel_scale) }, \
         { "k_att_filter", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_sensor_cal_t, k_att_filter) }, \
         { "k_gyro_bias", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_sensor_cal_t, k_gyro_bias) }, \
         } \
}


/**
 * @brief Pack a sensor_cal message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param gyro_scale Gyro scale.
 * @param accel_scale Accelerometer scale.
 * @param k_att_filter Gain on the complimentary attitude filter.
 * @param k_gyro_bias Gain on the gyro bias estimator.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_sensor_cal_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
						       float gyro_scale, float accel_scale, float k_att_filter, float k_gyro_bias)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_SENSOR_CAL_LEN];
	_mav_put_float(buf, 0, gyro_scale);
	_mav_put_float(buf, 4, accel_scale);
	_mav_put_float(buf, 8, k_att_filter);
	_mav_put_float(buf, 12, k_gyro_bias);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_SENSOR_CAL_LEN);
#else
	mavlink_sensor_cal_t packet;
	packet.gyro_scale = gyro_scale;
	packet.accel_scale = accel_scale;
	packet.k_att_filter = k_att_filter;
	packet.k_gyro_bias = k_gyro_bias;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_SENSOR_CAL_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_SENSOR_CAL;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_SENSOR_CAL_LEN, MAVLINK_MSG_ID_SENSOR_CAL_CRC);
#else
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_SENSOR_CAL_LEN);
#endif
}

/**
 * @brief Pack a sensor_cal message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param gyro_scale Gyro scale.
 * @param accel_scale Accelerometer scale.
 * @param k_att_filter Gain on the complimentary attitude filter.
 * @param k_gyro_bias Gain on the gyro bias estimator.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_sensor_cal_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
							   mavlink_message_t* msg,
						           float gyro_scale,float accel_scale,float k_att_filter,float k_gyro_bias)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_SENSOR_CAL_LEN];
	_mav_put_float(buf, 0, gyro_scale);
	_mav_put_float(buf, 4, accel_scale);
	_mav_put_float(buf, 8, k_att_filter);
	_mav_put_float(buf, 12, k_gyro_bias);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_SENSOR_CAL_LEN);
#else
	mavlink_sensor_cal_t packet;
	packet.gyro_scale = gyro_scale;
	packet.accel_scale = accel_scale;
	packet.k_att_filter = k_att_filter;
	packet.k_gyro_bias = k_gyro_bias;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_SENSOR_CAL_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_SENSOR_CAL;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_SENSOR_CAL_LEN, MAVLINK_MSG_ID_SENSOR_CAL_CRC);
#else
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_SENSOR_CAL_LEN);
#endif
}

/**
 * @brief Encode a sensor_cal struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param sensor_cal C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_sensor_cal_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_sensor_cal_t* sensor_cal)
{
	return mavlink_msg_sensor_cal_pack(system_id, component_id, msg, sensor_cal->gyro_scale, sensor_cal->accel_scale, sensor_cal->k_att_filter, sensor_cal->k_gyro_bias);
}

/**
 * @brief Encode a sensor_cal struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param sensor_cal C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_sensor_cal_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_sensor_cal_t* sensor_cal)
{
	return mavlink_msg_sensor_cal_pack_chan(system_id, component_id, chan, msg, sensor_cal->gyro_scale, sensor_cal->accel_scale, sensor_cal->k_att_filter, sensor_cal->k_gyro_bias);
}

/**
 * @brief Send a sensor_cal message
 * @param chan MAVLink channel to send the message
 *
 * @param gyro_scale Gyro scale.
 * @param accel_scale Accelerometer scale.
 * @param k_att_filter Gain on the complimentary attitude filter.
 * @param k_gyro_bias Gain on the gyro bias estimator.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_sensor_cal_send(mavlink_channel_t chan, float gyro_scale, float accel_scale, float k_att_filter, float k_gyro_bias)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_SENSOR_CAL_LEN];
	_mav_put_float(buf, 0, gyro_scale);
	_mav_put_float(buf, 4, accel_scale);
	_mav_put_float(buf, 8, k_att_filter);
	_mav_put_float(buf, 12, k_gyro_bias);

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SENSOR_CAL, buf, MAVLINK_MSG_ID_SENSOR_CAL_LEN, MAVLINK_MSG_ID_SENSOR_CAL_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SENSOR_CAL, buf, MAVLINK_MSG_ID_SENSOR_CAL_LEN);
#endif
#else
	mavlink_sensor_cal_t packet;
	packet.gyro_scale = gyro_scale;
	packet.accel_scale = accel_scale;
	packet.k_att_filter = k_att_filter;
	packet.k_gyro_bias = k_gyro_bias;

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SENSOR_CAL, (const char *)&packet, MAVLINK_MSG_ID_SENSOR_CAL_LEN, MAVLINK_MSG_ID_SENSOR_CAL_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SENSOR_CAL, (const char *)&packet, MAVLINK_MSG_ID_SENSOR_CAL_LEN);
#endif
#endif
}

#if MAVLINK_MSG_ID_SENSOR_CAL_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_sensor_cal_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  float gyro_scale, float accel_scale, float k_att_filter, float k_gyro_bias)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char *buf = (char *)msgbuf;
	_mav_put_float(buf, 0, gyro_scale);
	_mav_put_float(buf, 4, accel_scale);
	_mav_put_float(buf, 8, k_att_filter);
	_mav_put_float(buf, 12, k_gyro_bias);

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SENSOR_CAL, buf, MAVLINK_MSG_ID_SENSOR_CAL_LEN, MAVLINK_MSG_ID_SENSOR_CAL_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SENSOR_CAL, buf, MAVLINK_MSG_ID_SENSOR_CAL_LEN);
#endif
#else
	mavlink_sensor_cal_t *packet = (mavlink_sensor_cal_t *)msgbuf;
	packet->gyro_scale = gyro_scale;
	packet->accel_scale = accel_scale;
	packet->k_att_filter = k_att_filter;
	packet->k_gyro_bias = k_gyro_bias;

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SENSOR_CAL, (const char *)packet, MAVLINK_MSG_ID_SENSOR_CAL_LEN, MAVLINK_MSG_ID_SENSOR_CAL_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SENSOR_CAL, (const char *)packet, MAVLINK_MSG_ID_SENSOR_CAL_LEN);
#endif
#endif
}
#endif

#endif

// MESSAGE SENSOR_CAL UNPACKING


/**
 * @brief Get field gyro_scale from sensor_cal message
 *
 * @return Gyro scale.
 */
static inline float mavlink_msg_sensor_cal_get_gyro_scale(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  0);
}

/**
 * @brief Get field accel_scale from sensor_cal message
 *
 * @return Accelerometer scale.
 */
static inline float mavlink_msg_sensor_cal_get_accel_scale(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  4);
}

/**
 * @brief Get field k_att_filter from sensor_cal message
 *
 * @return Gain on the complimentary attitude filter.
 */
static inline float mavlink_msg_sensor_cal_get_k_att_filter(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  8);
}

/**
 * @brief Get field k_gyro_bias from sensor_cal message
 *
 * @return Gain on the gyro bias estimator.
 */
static inline float mavlink_msg_sensor_cal_get_k_gyro_bias(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  12);
}

/**
 * @brief Decode a sensor_cal message into a struct
 *
 * @param msg The message to decode
 * @param sensor_cal C-struct to decode the message contents into
 */
static inline void mavlink_msg_sensor_cal_decode(const mavlink_message_t* msg, mavlink_sensor_cal_t* sensor_cal)
{
#if MAVLINK_NEED_BYTE_SWAP
	sensor_cal->gyro_scale = mavlink_msg_sensor_cal_get_gyro_scale(msg);
	sensor_cal->accel_scale = mavlink_msg_sensor_cal_get_accel_scale(msg);
	sensor_cal->k_att_filter = mavlink_msg_sensor_cal_get_k_att_filter(msg);
	sensor_cal->k_gyro_bias = mavlink_msg_sensor_cal_get_k_gyro_bias(msg);
#else
	memcpy(sensor_cal, _MAV_PAYLOAD(msg), MAVLINK_MSG_ID_SENSOR_CAL_LEN);
#endif
}
