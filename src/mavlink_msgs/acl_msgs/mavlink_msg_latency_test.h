// MESSAGE LATENCY_TEST PACKING

#define MAVLINK_MSG_ID_LATENCY_TEST 155

typedef struct __mavlink_latency_test_t
{
 double send_time; ///< Time in seconds that the message was sent.
 int64_t fill1; ///< Filler1.
 int32_t fill2; ///< Filler2.
 int16_t fill3; ///< Filler3.
} mavlink_latency_test_t;

#define MAVLINK_MSG_ID_LATENCY_TEST_LEN 22
#define MAVLINK_MSG_ID_155_LEN 22

#define MAVLINK_MSG_ID_LATENCY_TEST_CRC 73
#define MAVLINK_MSG_ID_155_CRC 73



#define MAVLINK_MESSAGE_INFO_LATENCY_TEST { \
	"LATENCY_TEST", \
	4, \
	{  { "send_time", NULL, MAVLINK_TYPE_DOUBLE, 0, 0, offsetof(mavlink_latency_test_t, send_time) }, \
         { "fill1", NULL, MAVLINK_TYPE_INT64_T, 0, 8, offsetof(mavlink_latency_test_t, fill1) }, \
         { "fill2", NULL, MAVLINK_TYPE_INT32_T, 0, 16, offsetof(mavlink_latency_test_t, fill2) }, \
         { "fill3", NULL, MAVLINK_TYPE_INT16_T, 0, 20, offsetof(mavlink_latency_test_t, fill3) }, \
         } \
}


/**
 * @brief Pack a latency_test message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param send_time Time in seconds that the message was sent.
 * @param fill1 Filler1.
 * @param fill2 Filler2.
 * @param fill3 Filler3.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_latency_test_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
						       double send_time, int64_t fill1, int32_t fill2, int16_t fill3)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_LATENCY_TEST_LEN];
	_mav_put_double(buf, 0, send_time);
	_mav_put_int64_t(buf, 8, fill1);
	_mav_put_int32_t(buf, 16, fill2);
	_mav_put_int16_t(buf, 20, fill3);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_LATENCY_TEST_LEN);
#else
	mavlink_latency_test_t packet;
	packet.send_time = send_time;
	packet.fill1 = fill1;
	packet.fill2 = fill2;
	packet.fill3 = fill3;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_LATENCY_TEST_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_LATENCY_TEST;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_LATENCY_TEST_LEN, MAVLINK_MSG_ID_LATENCY_TEST_CRC);
#else
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_LATENCY_TEST_LEN);
#endif
}

/**
 * @brief Pack a latency_test message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param send_time Time in seconds that the message was sent.
 * @param fill1 Filler1.
 * @param fill2 Filler2.
 * @param fill3 Filler3.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_latency_test_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
							   mavlink_message_t* msg,
						           double send_time,int64_t fill1,int32_t fill2,int16_t fill3)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_LATENCY_TEST_LEN];
	_mav_put_double(buf, 0, send_time);
	_mav_put_int64_t(buf, 8, fill1);
	_mav_put_int32_t(buf, 16, fill2);
	_mav_put_int16_t(buf, 20, fill3);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_LATENCY_TEST_LEN);
#else
	mavlink_latency_test_t packet;
	packet.send_time = send_time;
	packet.fill1 = fill1;
	packet.fill2 = fill2;
	packet.fill3 = fill3;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_LATENCY_TEST_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_LATENCY_TEST;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_LATENCY_TEST_LEN, MAVLINK_MSG_ID_LATENCY_TEST_CRC);
#else
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_LATENCY_TEST_LEN);
#endif
}

/**
 * @brief Encode a latency_test struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param latency_test C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_latency_test_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_latency_test_t* latency_test)
{
	return mavlink_msg_latency_test_pack(system_id, component_id, msg, latency_test->send_time, latency_test->fill1, latency_test->fill2, latency_test->fill3);
}

/**
 * @brief Encode a latency_test struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param latency_test C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_latency_test_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_latency_test_t* latency_test)
{
	return mavlink_msg_latency_test_pack_chan(system_id, component_id, chan, msg, latency_test->send_time, latency_test->fill1, latency_test->fill2, latency_test->fill3);
}

/**
 * @brief Send a latency_test message
 * @param chan MAVLink channel to send the message
 *
 * @param send_time Time in seconds that the message was sent.
 * @param fill1 Filler1.
 * @param fill2 Filler2.
 * @param fill3 Filler3.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_latency_test_send(mavlink_channel_t chan, double send_time, int64_t fill1, int32_t fill2, int16_t fill3)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_LATENCY_TEST_LEN];
	_mav_put_double(buf, 0, send_time);
	_mav_put_int64_t(buf, 8, fill1);
	_mav_put_int32_t(buf, 16, fill2);
	_mav_put_int16_t(buf, 20, fill3);

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_LATENCY_TEST, buf, MAVLINK_MSG_ID_LATENCY_TEST_LEN, MAVLINK_MSG_ID_LATENCY_TEST_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_LATENCY_TEST, buf, MAVLINK_MSG_ID_LATENCY_TEST_LEN);
#endif
#else
	mavlink_latency_test_t packet;
	packet.send_time = send_time;
	packet.fill1 = fill1;
	packet.fill2 = fill2;
	packet.fill3 = fill3;

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_LATENCY_TEST, (const char *)&packet, MAVLINK_MSG_ID_LATENCY_TEST_LEN, MAVLINK_MSG_ID_LATENCY_TEST_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_LATENCY_TEST, (const char *)&packet, MAVLINK_MSG_ID_LATENCY_TEST_LEN);
#endif
#endif
}

#if MAVLINK_MSG_ID_LATENCY_TEST_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_latency_test_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  double send_time, int64_t fill1, int32_t fill2, int16_t fill3)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char *buf = (char *)msgbuf;
	_mav_put_double(buf, 0, send_time);
	_mav_put_int64_t(buf, 8, fill1);
	_mav_put_int32_t(buf, 16, fill2);
	_mav_put_int16_t(buf, 20, fill3);

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_LATENCY_TEST, buf, MAVLINK_MSG_ID_LATENCY_TEST_LEN, MAVLINK_MSG_ID_LATENCY_TEST_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_LATENCY_TEST, buf, MAVLINK_MSG_ID_LATENCY_TEST_LEN);
#endif
#else
	mavlink_latency_test_t *packet = (mavlink_latency_test_t *)msgbuf;
	packet->send_time = send_time;
	packet->fill1 = fill1;
	packet->fill2 = fill2;
	packet->fill3 = fill3;

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_LATENCY_TEST, (const char *)packet, MAVLINK_MSG_ID_LATENCY_TEST_LEN, MAVLINK_MSG_ID_LATENCY_TEST_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_LATENCY_TEST, (const char *)packet, MAVLINK_MSG_ID_LATENCY_TEST_LEN);
#endif
#endif
}
#endif

#endif

// MESSAGE LATENCY_TEST UNPACKING


/**
 * @brief Get field send_time from latency_test message
 *
 * @return Time in seconds that the message was sent.
 */
static inline double mavlink_msg_latency_test_get_send_time(const mavlink_message_t* msg)
{
	return _MAV_RETURN_double(msg,  0);
}

/**
 * @brief Get field fill1 from latency_test message
 *
 * @return Filler1.
 */
static inline int64_t mavlink_msg_latency_test_get_fill1(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int64_t(msg,  8);
}

/**
 * @brief Get field fill2 from latency_test message
 *
 * @return Filler2.
 */
static inline int32_t mavlink_msg_latency_test_get_fill2(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int32_t(msg,  16);
}

/**
 * @brief Get field fill3 from latency_test message
 *
 * @return Filler3.
 */
static inline int16_t mavlink_msg_latency_test_get_fill3(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int16_t(msg,  20);
}

/**
 * @brief Decode a latency_test message into a struct
 *
 * @param msg The message to decode
 * @param latency_test C-struct to decode the message contents into
 */
static inline void mavlink_msg_latency_test_decode(const mavlink_message_t* msg, mavlink_latency_test_t* latency_test)
{
#if MAVLINK_NEED_BYTE_SWAP
	latency_test->send_time = mavlink_msg_latency_test_get_send_time(msg);
	latency_test->fill1 = mavlink_msg_latency_test_get_fill1(msg);
	latency_test->fill2 = mavlink_msg_latency_test_get_fill2(msg);
	latency_test->fill3 = mavlink_msg_latency_test_get_fill3(msg);
#else
	memcpy(latency_test, _MAV_PAYLOAD(msg), MAVLINK_MSG_ID_LATENCY_TEST_LEN);
#endif
}
