// MESSAGE SENSORS PACKING

#define MAVLINK_MSG_ID_SENSORS 154

typedef struct __mavlink_sensors_t
{
 float gyro_x; ///< Gyro x.
 float gyro_y; ///< Gyro y.
 float gyro_z; ///< Gyro z.
 float accel_x; ///< Accel x.
 float accel_y; ///< Accel y.
 float accel_z; ///< Accel z.
 float mag_x; ///< Mag x.
 float mag_y; ///< Mag y.
 float mag_z; ///< Mag z.
 float pressure; ///< Pressure.
 float temperature; ///< Temperature.
} mavlink_sensors_t;

#define MAVLINK_MSG_ID_SENSORS_LEN 44
#define MAVLINK_MSG_ID_154_LEN 44

#define MAVLINK_MSG_ID_SENSORS_CRC 146
#define MAVLINK_MSG_ID_154_CRC 146



#define MAVLINK_MESSAGE_INFO_SENSORS { \
	"SENSORS", \
	11, \
	{  { "gyro_x", NULL, MAVLINK_TYPE_FLOAT, 0, 0, offsetof(mavlink_sensors_t, gyro_x) }, \
         { "gyro_y", NULL, MAVLINK_TYPE_FLOAT, 0, 4, offsetof(mavlink_sensors_t, gyro_y) }, \
         { "gyro_z", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_sensors_t, gyro_z) }, \
         { "accel_x", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_sensors_t, accel_x) }, \
         { "accel_y", NULL, MAVLINK_TYPE_FLOAT, 0, 16, offsetof(mavlink_sensors_t, accel_y) }, \
         { "accel_z", NULL, MAVLINK_TYPE_FLOAT, 0, 20, offsetof(mavlink_sensors_t, accel_z) }, \
         { "mag_x", NULL, MAVLINK_TYPE_FLOAT, 0, 24, offsetof(mavlink_sensors_t, mag_x) }, \
         { "mag_y", NULL, MAVLINK_TYPE_FLOAT, 0, 28, offsetof(mavlink_sensors_t, mag_y) }, \
         { "mag_z", NULL, MAVLINK_TYPE_FLOAT, 0, 32, offsetof(mavlink_sensors_t, mag_z) }, \
         { "pressure", NULL, MAVLINK_TYPE_FLOAT, 0, 36, offsetof(mavlink_sensors_t, pressure) }, \
         { "temperature", NULL, MAVLINK_TYPE_FLOAT, 0, 40, offsetof(mavlink_sensors_t, temperature) }, \
         } \
}


/**
 * @brief Pack a sensors message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param gyro_x Gyro x.
 * @param gyro_y Gyro y.
 * @param gyro_z Gyro z.
 * @param accel_x Accel x.
 * @param accel_y Accel y.
 * @param accel_z Accel z.
 * @param mag_x Mag x.
 * @param mag_y Mag y.
 * @param mag_z Mag z.
 * @param pressure Pressure.
 * @param temperature Temperature.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_sensors_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
						       float gyro_x, float gyro_y, float gyro_z, float accel_x, float accel_y, float accel_z, float mag_x, float mag_y, float mag_z, float pressure, float temperature)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_SENSORS_LEN];
	_mav_put_float(buf, 0, gyro_x);
	_mav_put_float(buf, 4, gyro_y);
	_mav_put_float(buf, 8, gyro_z);
	_mav_put_float(buf, 12, accel_x);
	_mav_put_float(buf, 16, accel_y);
	_mav_put_float(buf, 20, accel_z);
	_mav_put_float(buf, 24, mag_x);
	_mav_put_float(buf, 28, mag_y);
	_mav_put_float(buf, 32, mag_z);
	_mav_put_float(buf, 36, pressure);
	_mav_put_float(buf, 40, temperature);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_SENSORS_LEN);
#else
	mavlink_sensors_t packet;
	packet.gyro_x = gyro_x;
	packet.gyro_y = gyro_y;
	packet.gyro_z = gyro_z;
	packet.accel_x = accel_x;
	packet.accel_y = accel_y;
	packet.accel_z = accel_z;
	packet.mag_x = mag_x;
	packet.mag_y = mag_y;
	packet.mag_z = mag_z;
	packet.pressure = pressure;
	packet.temperature = temperature;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_SENSORS_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_SENSORS;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_SENSORS_LEN, MAVLINK_MSG_ID_SENSORS_CRC);
#else
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_SENSORS_LEN);
#endif
}

/**
 * @brief Pack a sensors message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param gyro_x Gyro x.
 * @param gyro_y Gyro y.
 * @param gyro_z Gyro z.
 * @param accel_x Accel x.
 * @param accel_y Accel y.
 * @param accel_z Accel z.
 * @param mag_x Mag x.
 * @param mag_y Mag y.
 * @param mag_z Mag z.
 * @param pressure Pressure.
 * @param temperature Temperature.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_sensors_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
							   mavlink_message_t* msg,
						           float gyro_x,float gyro_y,float gyro_z,float accel_x,float accel_y,float accel_z,float mag_x,float mag_y,float mag_z,float pressure,float temperature)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_SENSORS_LEN];
	_mav_put_float(buf, 0, gyro_x);
	_mav_put_float(buf, 4, gyro_y);
	_mav_put_float(buf, 8, gyro_z);
	_mav_put_float(buf, 12, accel_x);
	_mav_put_float(buf, 16, accel_y);
	_mav_put_float(buf, 20, accel_z);
	_mav_put_float(buf, 24, mag_x);
	_mav_put_float(buf, 28, mag_y);
	_mav_put_float(buf, 32, mag_z);
	_mav_put_float(buf, 36, pressure);
	_mav_put_float(buf, 40, temperature);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_SENSORS_LEN);
#else
	mavlink_sensors_t packet;
	packet.gyro_x = gyro_x;
	packet.gyro_y = gyro_y;
	packet.gyro_z = gyro_z;
	packet.accel_x = accel_x;
	packet.accel_y = accel_y;
	packet.accel_z = accel_z;
	packet.mag_x = mag_x;
	packet.mag_y = mag_y;
	packet.mag_z = mag_z;
	packet.pressure = pressure;
	packet.temperature = temperature;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_SENSORS_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_SENSORS;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_SENSORS_LEN, MAVLINK_MSG_ID_SENSORS_CRC);
#else
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_SENSORS_LEN);
#endif
}

/**
 * @brief Encode a sensors struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param sensors C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_sensors_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_sensors_t* sensors)
{
	return mavlink_msg_sensors_pack(system_id, component_id, msg, sensors->gyro_x, sensors->gyro_y, sensors->gyro_z, sensors->accel_x, sensors->accel_y, sensors->accel_z, sensors->mag_x, sensors->mag_y, sensors->mag_z, sensors->pressure, sensors->temperature);
}

/**
 * @brief Encode a sensors struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param sensors C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_sensors_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_sensors_t* sensors)
{
	return mavlink_msg_sensors_pack_chan(system_id, component_id, chan, msg, sensors->gyro_x, sensors->gyro_y, sensors->gyro_z, sensors->accel_x, sensors->accel_y, sensors->accel_z, sensors->mag_x, sensors->mag_y, sensors->mag_z, sensors->pressure, sensors->temperature);
}

/**
 * @brief Send a sensors message
 * @param chan MAVLink channel to send the message
 *
 * @param gyro_x Gyro x.
 * @param gyro_y Gyro y.
 * @param gyro_z Gyro z.
 * @param accel_x Accel x.
 * @param accel_y Accel y.
 * @param accel_z Accel z.
 * @param mag_x Mag x.
 * @param mag_y Mag y.
 * @param mag_z Mag z.
 * @param pressure Pressure.
 * @param temperature Temperature.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_sensors_send(mavlink_channel_t chan, float gyro_x, float gyro_y, float gyro_z, float accel_x, float accel_y, float accel_z, float mag_x, float mag_y, float mag_z, float pressure, float temperature)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_SENSORS_LEN];
	_mav_put_float(buf, 0, gyro_x);
	_mav_put_float(buf, 4, gyro_y);
	_mav_put_float(buf, 8, gyro_z);
	_mav_put_float(buf, 12, accel_x);
	_mav_put_float(buf, 16, accel_y);
	_mav_put_float(buf, 20, accel_z);
	_mav_put_float(buf, 24, mag_x);
	_mav_put_float(buf, 28, mag_y);
	_mav_put_float(buf, 32, mag_z);
	_mav_put_float(buf, 36, pressure);
	_mav_put_float(buf, 40, temperature);

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SENSORS, buf, MAVLINK_MSG_ID_SENSORS_LEN, MAVLINK_MSG_ID_SENSORS_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SENSORS, buf, MAVLINK_MSG_ID_SENSORS_LEN);
#endif
#else
	mavlink_sensors_t packet;
	packet.gyro_x = gyro_x;
	packet.gyro_y = gyro_y;
	packet.gyro_z = gyro_z;
	packet.accel_x = accel_x;
	packet.accel_y = accel_y;
	packet.accel_z = accel_z;
	packet.mag_x = mag_x;
	packet.mag_y = mag_y;
	packet.mag_z = mag_z;
	packet.pressure = pressure;
	packet.temperature = temperature;

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SENSORS, (const char *)&packet, MAVLINK_MSG_ID_SENSORS_LEN, MAVLINK_MSG_ID_SENSORS_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SENSORS, (const char *)&packet, MAVLINK_MSG_ID_SENSORS_LEN);
#endif
#endif
}

#if MAVLINK_MSG_ID_SENSORS_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_sensors_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  float gyro_x, float gyro_y, float gyro_z, float accel_x, float accel_y, float accel_z, float mag_x, float mag_y, float mag_z, float pressure, float temperature)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char *buf = (char *)msgbuf;
	_mav_put_float(buf, 0, gyro_x);
	_mav_put_float(buf, 4, gyro_y);
	_mav_put_float(buf, 8, gyro_z);
	_mav_put_float(buf, 12, accel_x);
	_mav_put_float(buf, 16, accel_y);
	_mav_put_float(buf, 20, accel_z);
	_mav_put_float(buf, 24, mag_x);
	_mav_put_float(buf, 28, mag_y);
	_mav_put_float(buf, 32, mag_z);
	_mav_put_float(buf, 36, pressure);
	_mav_put_float(buf, 40, temperature);

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SENSORS, buf, MAVLINK_MSG_ID_SENSORS_LEN, MAVLINK_MSG_ID_SENSORS_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SENSORS, buf, MAVLINK_MSG_ID_SENSORS_LEN);
#endif
#else
	mavlink_sensors_t *packet = (mavlink_sensors_t *)msgbuf;
	packet->gyro_x = gyro_x;
	packet->gyro_y = gyro_y;
	packet->gyro_z = gyro_z;
	packet->accel_x = accel_x;
	packet->accel_y = accel_y;
	packet->accel_z = accel_z;
	packet->mag_x = mag_x;
	packet->mag_y = mag_y;
	packet->mag_z = mag_z;
	packet->pressure = pressure;
	packet->temperature = temperature;

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SENSORS, (const char *)packet, MAVLINK_MSG_ID_SENSORS_LEN, MAVLINK_MSG_ID_SENSORS_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SENSORS, (const char *)packet, MAVLINK_MSG_ID_SENSORS_LEN);
#endif
#endif
}
#endif

#endif

// MESSAGE SENSORS UNPACKING


/**
 * @brief Get field gyro_x from sensors message
 *
 * @return Gyro x.
 */
static inline float mavlink_msg_sensors_get_gyro_x(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  0);
}

/**
 * @brief Get field gyro_y from sensors message
 *
 * @return Gyro y.
 */
static inline float mavlink_msg_sensors_get_gyro_y(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  4);
}

/**
 * @brief Get field gyro_z from sensors message
 *
 * @return Gyro z.
 */
static inline float mavlink_msg_sensors_get_gyro_z(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  8);
}

/**
 * @brief Get field accel_x from sensors message
 *
 * @return Accel x.
 */
static inline float mavlink_msg_sensors_get_accel_x(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  12);
}

/**
 * @brief Get field accel_y from sensors message
 *
 * @return Accel y.
 */
static inline float mavlink_msg_sensors_get_accel_y(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  16);
}

/**
 * @brief Get field accel_z from sensors message
 *
 * @return Accel z.
 */
static inline float mavlink_msg_sensors_get_accel_z(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  20);
}

/**
 * @brief Get field mag_x from sensors message
 *
 * @return Mag x.
 */
static inline float mavlink_msg_sensors_get_mag_x(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  24);
}

/**
 * @brief Get field mag_y from sensors message
 *
 * @return Mag y.
 */
static inline float mavlink_msg_sensors_get_mag_y(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  28);
}

/**
 * @brief Get field mag_z from sensors message
 *
 * @return Mag z.
 */
static inline float mavlink_msg_sensors_get_mag_z(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  32);
}

/**
 * @brief Get field pressure from sensors message
 *
 * @return Pressure.
 */
static inline float mavlink_msg_sensors_get_pressure(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  36);
}

/**
 * @brief Get field temperature from sensors message
 *
 * @return Temperature.
 */
static inline float mavlink_msg_sensors_get_temperature(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  40);
}

/**
 * @brief Decode a sensors message into a struct
 *
 * @param msg The message to decode
 * @param sensors C-struct to decode the message contents into
 */
static inline void mavlink_msg_sensors_decode(const mavlink_message_t* msg, mavlink_sensors_t* sensors)
{
#if MAVLINK_NEED_BYTE_SWAP
	sensors->gyro_x = mavlink_msg_sensors_get_gyro_x(msg);
	sensors->gyro_y = mavlink_msg_sensors_get_gyro_y(msg);
	sensors->gyro_z = mavlink_msg_sensors_get_gyro_z(msg);
	sensors->accel_x = mavlink_msg_sensors_get_accel_x(msg);
	sensors->accel_y = mavlink_msg_sensors_get_accel_y(msg);
	sensors->accel_z = mavlink_msg_sensors_get_accel_z(msg);
	sensors->mag_x = mavlink_msg_sensors_get_mag_x(msg);
	sensors->mag_y = mavlink_msg_sensors_get_mag_y(msg);
	sensors->mag_z = mavlink_msg_sensors_get_mag_z(msg);
	sensors->pressure = mavlink_msg_sensors_get_pressure(msg);
	sensors->temperature = mavlink_msg_sensors_get_temperature(msg);
#else
	memcpy(sensors, _MAV_PAYLOAD(msg), MAVLINK_MSG_ID_SENSORS_LEN);
#endif
}
