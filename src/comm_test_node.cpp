/*
 * comm_test_node.cpp
 *
 *  Created on: Dec 22, 2014
 *      Author: mark
 */

#include <iostream>

// ROS includes
#include "ros/ros.h"
#include "comm_test/CommStats.h"

// acl utils library
#include "acl/comms/serialPort.hpp"
#include "acl/utils.hpp"

/* This assumes you have the mavlink headers on your include path
 or in the same folder as this source file */
#include "mavlink_msgs/acl_msgs/mavlink.h"

#define BUFFER_LENGTH 2041 // minimum buffer size that can be used with qnx (I don't know why)
static int packet_drops = 0;
int sp1 = 0;
int sp2 = 0;


void *serListen(void *serial_port)
{
	acl::SerialPort * ser = (acl::SerialPort *) serial_port;
	int last_seq = -1;

	ros::NodeHandle n;

	comm_test::CommStats comm_stats;
	ros::Publisher pub = n.advertise<comm_test::CommStats>("comm_stats", 1);


	while (ros::ok())
	{

		mavlink_message_t msg;
		mavlink_status_t status;

		// COMMUNICATION THROUGH EXTERNAL UART PORT (XBee serial)

		uint8_t c = ser->spReceiveSingle();

		// Try to get a new message
		if (mavlink_parse_char(MAVLINK_COMM_0, c, &msg, &status))
		{


			// Handle message
			mavlink_latency_test_t latency;
			switch (msg.msgid)
			{
			case MAVLINK_MSG_ID_LATENCY_TEST:
			{
				mavlink_msg_latency_test_decode(&msg, &latency);

				packet_drops += (uint8_t) (msg.seq - last_seq - 1);
				last_seq = msg.seq;

				comm_stats.latency = ros::Time::now().toSec() - latency.send_time;
				comm_stats.dropped_packets = packet_drops;
				comm_stats.xbee1 = sp1;
				comm_stats.xbee2 = sp2;
				pub.publish(comm_stats);

				std::cout << "Got message. Dropped packets: " << packet_drops << ", Latency: " << comm_stats.latency << std::endl;

			}
				break;
			default:
				//Do nothing
				break;
			}

		}

		packet_drops += status.packet_rx_drop_count;



	}

}

int main(int argc, char* argv[])
{

	// initialize ros node
	ros::init(argc, argv, "comm_test", ros::init_options::AnonymousName);
	ros::NodeHandle n;

	// default settings
	int baudRate = 57600;
	std::string serialPort1 = "/dev/ttyUSB";
	std::string serialPort2 = "/dev/ttyUSB"; // default to 1-way comms
	bool two_dir = false;

	// parse inputs
	if (argc == 3)
	{
		baudRate = atoi(argv[1]);
		sp1 = atoi(argv[2]);
		sp2 = sp1;
	}
	if (argc == 4)
	{
		two_dir = true;
		baudRate = atoi(argv[1]);
		sp1 = atoi(argv[2]);
		sp2 = atoi(argv[3]);
	}

	serialPort1.append(std::to_string(sp1));
	serialPort2.append(std::to_string(sp2));

	uint8_t buf[BUFFER_LENGTH];
	ssize_t recsize;
	socklen_t fromlen;
	int bytes_sent;
	mavlink_message_t msg;
	uint16_t len;

	// setup serial communication
	acl::SerialPort ser1, ser2;
	if (!ser1.spInitialize(serialPort1.c_str(), baudRate, true))
	{
		ROS_ERROR("Serial port failed to open");
		return 0;
	}
	ROS_INFO_STREAM("Serial port " << serialPort1 << " started at " << baudRate << " bits/sec\n");
	if (two_dir)
	{
		if (!ser2.spInitialize(serialPort2.c_str(), baudRate, true))
		{
			ROS_ERROR("Serial port failed to open");
			return 0;
		}
		ROS_INFO_STREAM("Serial port " << serialPort2 << " started at " << baudRate << " bits/sec\n");
	}
	//## Start a serial listen thread
	pthread_t threads;
	if (two_dir)
	{
		if (pthread_create(&threads, NULL, serListen, (void *) &ser2))
			ROS_ERROR("Serial listen thread failed to start");
	} else
	{
		if (pthread_create(&threads, NULL, serListen, (void *) &ser1))
			ROS_ERROR("Serial listen thread failed to start");
	}

	while(ros::ok())
	{

		/*Send Latency test */
		mavlink_msg_latency_test_pack(1, 200, &msg, ros::Time::now().toSec(), 1, 2, 3);
		len = mavlink_msg_to_send_buffer(buf, &msg);
		bytes_sent = ser1.spSend(buf, len);

		memset(buf, 0, BUFFER_LENGTH);
		//sleep(1);  // Sleep one second
		usleep(10000);
	}

	return 1;
}

