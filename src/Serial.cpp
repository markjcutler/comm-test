/*
 * Serail.cpp
 *
 *  Created on: Jan 5, 2015
 *      Author: mark
 */

#include "Serial.h"

Serial::Serial(int baudRate, std::string serialPort)
{

	if (!ser.spInitialize(serialPort.c_str(), baudRate, true))
	{
		ROS_ERROR("Serial port failed to open");
		return;
	}
}

Serial::~Serial()
{
	ser.spClose();
}

int Serial::get_data(uint8_t *buf, int len)
{
	if (len > 0)
		buf[0] = ser.spReceiveSingle();
	else
		return 0;
	return 1;
}

int Serial::send_data(uint8_t *buf , int len)
{
	return ser.spSend(buf, len);
}
