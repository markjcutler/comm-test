/*
 * reply.cpp
 *
 *  Created on: Dec 22, 2014
 *      Author: mark
 */


#include <iostream>

// ROS includes
#include "ros/ros.h"

// acl utils library
#include "acl/serialPort.hpp"
#include "acl/utils.hpp"

/* This assumes you have the mavlink headers on your include path
 or in the same folder as this source file */
#include "mavlink_msgs/acl_msgs/mavlink.h"

#define BUFFER_LENGTH 2041 // minimum buffer size that can be used with qnx (I don't know why)


int main(int argc, char* argv[])
{

	// initialize ros node
	ros::init(argc, argv, "reply");
	ros::NodeHandle n;

	uint8_t buf[BUFFER_LENGTH];
	ssize_t recsize;
	socklen_t fromlen;
	int bytes_sent;
	mavlink_message_t msg;
	uint16_t len;

	// setup serial communication
	std::string serialPort = "/dev/ttyUSB3";
	int baudRate = 115200;
	acl::SerialPort ser;
	if (!ser.spInitialize(serialPort.c_str(), baudRate, true))
	{
		ROS_ERROR("Serial port failed to open");
	}

	while(ros::ok())
	{

		mavlink_message_t msg;
		mavlink_status_t status;

		// COMMUNICATION THROUGH EXTERNAL UART PORT (XBee serial)

		uint8_t c = ser.spReceiveSingle();
		// Try to get a new message
		if (mavlink_parse_char(MAVLINK_COMM_0, c, &msg, &status))
		{
			// Handle message

//			switch (msg.msgid)
//			{
//			case MAVLINK_MSG_ID_HEARTBEAT:
//			{
//				/*Send Heartbeat */
//				uint8_t buf[BUFFER_LENGTH];
//				mavlink_message_t msg2;
//				mavlink_msg_heartbeat_pack(1, 200, &msg2, MAV_TYPE_HELICOPTER,
//						MAV_AUTOPILOT_GENERIC, MAV_MODE_GUIDED_ARMED, 0,
//						MAV_STATE_ACTIVE);
//				uint16_t len = mavlink_msg_to_send_buffer(buf, &msg2);
//				bytes_sent = ser.spSend(buf, len);
//			}
//				break;
//			default:
//				//Do nothing
//				break;
//			}

		}



	}

	return 1;
}



