/*
 * CommTest.cpp
 *
 *  Created on: Jan 5, 2015
 *      Author: mark
 */

#include "CommTest.h"

CommTest::CommTest()
{
	packet_drops = 0;
	sent_messages = 0;

	// setup output file
	log_file.open("/home/mark/comm_log.txt");

}

CommTest::~CommTest()
{
  log_file.close();
	// TODO Auto-generated destructor stub
}

void CommTest::reply_packets()
{
    int packet_drops_reply = 0;
    int last_seq = -1;
    long long message_num = 0;

    while(ros::ok())
    {

        mavlink_message_t msg;
        mavlink_status_t status;

        // COMMUNICATION THROUGH EXTERNAL PORT
        uint8_t bufr[2048];
        int received_bytes = this->get_data(bufr, sizeof(bufr));
        // Try to get a new message
        for (int i=0; i<received_bytes; i++)
        {
            uint8_t c = bufr[i];

            // Try to get a new message
            if (mavlink_parse_char(MAVLINK_COMM_0, c, &msg, &status))
            {

            // Handle message
            mavlink_latency_test_t latency;
            switch (msg.msgid)
            {
            case MAVLINK_MSG_ID_LATENCY_TEST:
            {

                mavlink_msg_latency_test_decode(&msg, &latency);

                packet_drops_reply += (uint8_t) (latency.fill1 - last_seq - 1);
                last_seq = latency.fill1;

                double lat = ros::Time::now().toSec() - latency.send_time;

                message_num++;
                if (message_num % 20 == 0)
                    std::cout << "Got message number " << latency.fill1 << ". Dropped packets: " << packet_drops_reply << ", Latency: " << lat << std::endl;


                mavlink_msg_latency_test_decode(&msg, &latency);

                /*Send Return */
                uint8_t buf[BUFFER_LENGTH];
                mavlink_message_t msg2;
                mavlink_msg_latency_test_pack(1, 200, &msg2, latency.send_time, latency.fill1, 2, 3);
                uint16_t len = mavlink_msg_to_send_buffer(buf, &msg2);
                this->send_data(buf, len);
            }
            break;
            default:
                //Do nothing
                break;
            }

        }
        }



    }
}

void *listen(void *commtest)
{
	CommTest *ct = (CommTest*) commtest;
	int last_seq = -1;
	long long message_num = 0;

	ros::NodeHandle n;

	comm_test::CommStats comm_stats;
	ros::Publisher pub = n.advertise<comm_test::CommStats>("comm_stats", 1);


	while (ros::ok())
	{

		mavlink_message_t msg;
		mavlink_status_t status;

		// COMMUNICATION THROUGH EXTERNAL PORT
		uint8_t bufr[2048];
		int received_bytes = ct->get_data(bufr, sizeof(bufr));
		// Try to get a new message
		for (int i=0; i<received_bytes; i++)
		{
			uint8_t c = bufr[i];

			// Try to get a new message
			if (mavlink_parse_char(MAVLINK_COMM_0, c, &msg, &status))
			{

				// Handle message
				mavlink_latency_test_t latency;
				switch (msg.msgid)
				{
				case MAVLINK_MSG_ID_LATENCY_TEST:
				{
					mavlink_msg_latency_test_decode(&msg, &latency);

					ct->packet_drops += (uint8_t) (latency.fill1 - last_seq - 1);
					last_seq = latency.fill1;

					comm_stats.latency = ros::Time::now().toSec() - latency.send_time;
					comm_stats.dropped_packets = ct->packet_drops;
					comm_stats.xbee1 = 0;
					comm_stats.xbee2 = 1;
					pub.publish(comm_stats);

					if (message_num % 20 == 0)
						std::cout << "Got message number " << latency.fill1 << ". Dropped packets: " << ct->packet_drops << ", Latency: " << comm_stats.latency << std::endl;

					//write to file
					ct->log_file << message_num << "," << comm_stats.dropped_packets << "," << comm_stats.latency << "\n";
					message_num += 1;

				}
					break;
				default:
					//Do nothing
					break;
				}

			}

			//ct->packet_drops += status.packet_rx_drop_count;
		}
	}
	return NULL;
}


void CommTest::timer_callback(const ros::TimerEvent&)
{
	uint8_t buf[BUFFER_LENGTH];
	int bytes_sent;
	mavlink_message_t msg;
	uint16_t len;

	/*Send Latency test */
	mavlink_msg_latency_test_pack(1, 200, &msg, ros::Time::now().toSec(), sent_messages, 2, 3);
	len = mavlink_msg_to_send_buffer(buf, &msg);
	double start_time = ros::Time::now().toSec();
	bytes_sent = send_data(buf, len);
	//std::cout << sent_messages << ": took " << ros::Time::now().toSec() - start_time << " to send data" << std::endl;
	sent_messages += 1;
	if (sent_messages >= max_messages)
		ros::shutdown();

	memset(buf, 0, BUFFER_LENGTH);
}

void CommTest::send_packets(double rate, int max_messages)
{
	this->max_messages = max_messages;
	pthread_t threads;
	if (pthread_create(&threads, NULL, listen, this ))
		ROS_ERROR("Serial listen thread failed to start");

	ros::NodeHandle nh;
	ros::Timer timer = nh.createTimer(ros::Duration(rate), &CommTest::timer_callback, this);

	ros::spin();
}
