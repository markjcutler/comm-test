/*
 * Serial2.h
 *
 *  Created on: Jan 5, 2015
 *      Author: mark
 */

#ifndef SERIAL2_H_
#define SERIAL2_H_

#include "ros/ros.h"
#include "acl/comms/serialPort.hpp"
#include "CommTest.h"

class Serial2: public CommTest
{
public:
    Serial2(int baudRate, std::string serialPort1, std::string serialPort2);
    virtual ~Serial2();

    int get_data(uint8_t *, int);
    int send_data(uint8_t *, int);

    bool p1_init, p2_init;

private:
    acl::SerialPort ser1, ser2;
};

#endif /* SERIAL2_H_ */
