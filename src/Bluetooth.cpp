/*
 * Bluetooth.cpp
 *
 *  Created on: Jan 5, 2015
 *      Author: mark
 */

#include "Bluetooth.h"

Bluetooth::Bluetooth(std::string macaddr)
{

    if (!bt.BTInit(macaddr))
    {
        ROS_ERROR("Bluetooth port failed to open");
        return;
    }
}

Bluetooth::~Bluetooth()
{
    bt.BTClose();
}

int Bluetooth::get_data(uint8_t *buf, int len)
{
    if (len > 0)
        buf[0] = bt.BTReceiveByte();
    else
        return 0;
    return 1;
}

int Bluetooth::send_data(uint8_t *buf , int len)
{
    return bt.BTSend((char*) buf, len);
}
