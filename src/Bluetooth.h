/*
 * Bluetooth.h
 *
 *  Created on: Jan 5, 2015
 *      Author: mark
 */

#ifndef BLUETOOTH_H_
#define BLUETOOTH_H_

#include "ros/ros.h"
#include "acl/comms/BTPort.hpp"
#include "CommTest.h"

class Bluetooth: public CommTest
{
public:
    Bluetooth(std::string macaddr);
    virtual ~Bluetooth();

    int get_data(uint8_t *, int);
    int send_data(uint8_t *, int);

private:
    acl::BTPort bt;
};

#endif /* BLUETOOTH_H_ */
