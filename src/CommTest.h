/*
 * CommTest.h
 *
 *  Created on: Jan 5, 2015
 *      Author: mark
 */

#ifndef COMMTEST_H_
#define COMMTEST_H_

#include <iostream>
#include <fstream>

// ROS includes
#include "ros/ros.h"
#include "comm_test/CommStats.h"

// acl utils library
#include "acl/utils.hpp"

/* This assumes you have the mavlink headers on your include path
 or in the same folder as this source file */
#include "mavlink_msgs/acl_msgs/mavlink.h"

#define BUFFER_LENGTH 2048

void *listen(void *);
class CommTest
{
public:
	CommTest();
	virtual ~CommTest();
	virtual int get_data(uint8_t *, int) {return 0;};
	virtual int send_data(uint8_t *, int) {return 0;};

	void timer_callback(const ros::TimerEvent&);
	void send_packets(double rate, int max_messages);
        void reply_packets();
	int packet_drops;
	int sent_messages;
	int max_messages;
	std::ofstream log_file;
};

#endif /* COMMTEST_H_ */
