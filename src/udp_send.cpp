/*
 * comm_test_node.cpp
 *
 *  Created on: Dec 22, 2014
 *      Author: mark
 */

#include <iostream>

// ROS includes
#include "ros/ros.h"

#include "comm_test/CommStats.h"

// acl utils library
#include "acl/comms/udpPort.hpp"
#include "acl/utils.hpp"

/* This assumes you have the mavlink headers on your include path
 or in the same folder as this source file */
#include "mavlink_msgs/acl_msgs/mavlink.h"

#define BUFFER_LENGTH 2041 // minimum buffer size that can be used with qnx (I don't know why)
static int packet_drops = 0;
static double send_time = 0.0;


void *listen(void *comm)
{
	acl::UDPPort * udp = (acl::UDPPort *) comm;
	int last_seq = -1;

	while (1)
	{

		mavlink_message_t msg;
		mavlink_status_t status;

		// COMMUNICATION THROUGH EXTERNAL UART PORT (XBee serial)

		char buf[2048];
		int received_bytes = udp->udpReceive(buf, sizeof(buf));
		// Try to get a new message
		for (int i=0; i<received_bytes; i++)
		{
			uint8_t c = buf[i];
			if (mavlink_parse_char(MAVLINK_COMM_0, c, &msg, &status))
			{
				// Handle message

				switch (msg.msgid)
				{
				case MAVLINK_MSG_ID_LATENCY_TEST:
				{
					packet_drops += (uint8_t) (msg.seq - last_seq - 1);
					last_seq = msg.seq;

					std::cout << "Got message. Dropped packets: " << packet_drops << ", Latency: " << send_time - ros::Time::now().toSec() << std::endl;
				}
					break;
				default:
					//Do nothing
					break;
				}

			}

			packet_drops += status.packet_rx_drop_count;
		}


	}

}

int main(int argc, char* argv[])
{

	// initialize ros node
	ros::init(argc, argv, "comm_test");
	ros::NodeHandle n;

	comm_test::CommStats comm_stats;
	ros::Publisher pub = n.advertise<comm_test::CommStats>("comm_stats", 1);

	uint8_t buf[BUFFER_LENGTH];
	ssize_t recsize;
	socklen_t fromlen;
	int bytes_sent;
	mavlink_message_t msg;
	uint16_t len;

	// setup udp communication
	char *address = "192.167.2.51"; //"127.0.0.1";
	int rxport = 20051;
	int txport = 30051;
	acl::UDPPort udp;
	if (!udp.udpInit(true, address, rxport, txport))
	{
		ROS_ERROR("UDP port failed to open");
		return 0;
	}
//	else
//	{
//		//## Start a serial listen thread
//		pthread_t threads;
//		if (pthread_create(&threads, NULL, listen, (void *) &udp))
//			ROS_ERROR("Listen thread failed to start");
//	}

	int last_seq = -1;
	while(ros::ok())
	{

		/*Send Heartbeat */
//		mavlink_msg_heartbeat_pack(1, 200, &msg, MAV_TYPE_HELICOPTER,
//				MAV_AUTOPILOT_GENERIC, MAV_MODE_GUIDED_ARMED, 0,
//				MAV_STATE_ACTIVE);
		len = mavlink_msg_to_send_buffer(buf, &msg);
		send_time = ros::Time::now().toSec();
		bytes_sent = udp.udpSend(buf, len);

		memset(buf, 0, BUFFER_LENGTH);







		mavlink_message_t msg2;
		mavlink_status_t status2;

		// COMMUNICATION THROUGH EXTERNAL UART PORT (XBee serial)

		char buf[2048];
		int received_bytes = udp.udpReceive(buf, sizeof(buf));
		// Try to get a new message
		for (int i=0; i<received_bytes; i++)
		{
			uint8_t c = buf[i];
			if (mavlink_parse_char(MAVLINK_COMM_0, c, &msg2, &status2))
			{
				// Handle message

//				switch (msg2.msgid)
//				{
//				case MAVLINK_MSG_ID_HEARTBEAT:
//				{
//					packet_drops += (uint8_t) (msg2.seq - last_seq - 1);
//					last_seq = msg2.seq;
//
//					comm_stats.latency = ros::Time::now().toSec() - send_time;
//					comm_stats.dropped_packets = packet_drops;
//					pub.publish(comm_stats);
//					std::cout << "Got message. Dropped packets: " << packet_drops << ", Latency: " << comm_stats.latency << std::endl;
//
//				}
//					break;
//				default:
//					//Do nothing
//					break;
//				}

			}

			packet_drops += status2.packet_rx_drop_count;
		}









		//sleep(1);
		usleep(1000); // Sleep one second
	}

	return 1;
}

