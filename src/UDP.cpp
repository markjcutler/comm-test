/*
 * UDP.cpp
 *
 *  Created on: Jan 5, 2015
 *      Author: mark
 */

#include "UDP.h"

UDP::UDP(const char *address, int rxport, int txport)
{
    if (!udp.udpInit(true, (char*) address, rxport, txport))
    {
        ROS_ERROR("UDP port failed to open");
        return;
    }

    // set member variables
    this->address = address;
    this->rxport = rxport;
    this->txport = txport;
}

UDP::~UDP()
{
    // TODO Auto-generated destructor stub
}

int UDP::get_data(uint8_t *buf, int len)
{
    return udp.udpReceive((char *)buf, len);
}

int UDP::send_data(uint8_t *buf , int len)
{
    return udp.udpSend(buf, len);
}

bool UDP::set_ip_on_wifly(std::string ip_address)
{
    // need to telnet into wifly device and then set the ip address
    // just set up a socket using the address and txport

}

bool UDP::get_ip(int expected_address[], int subnet_length,
                 std::string &raven_ip)
{
    struct ifaddrs * ifAddrStruct=NULL;
    struct ifaddrs * ifa=NULL;
    void * tmpAddrPtr=NULL;

    getifaddrs(&ifAddrStruct);

    bool found_ip = true;
    printf("\n\n\nLooking for all IP addresses on this machine\n\n\n");
    for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
        if (!ifa->ifa_addr) {
            continue;
        }
        if (ifa->ifa_addr->sa_family == AF_INET) { // check it is IP4
            // is a valid IP4 Address
            tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
            char addressBuffer[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
            printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer);

            // parse out the ip4 address into a series of four numbers
            std::string ab(addressBuffer);
            std::istringstream iss(ab);
            std::string token;
            int cnt = 0;
            found_ip = true;
            while (std::getline(iss, token, '.')) {
                if (!token.empty() and cnt < subnet_length)
                {
                    int add_num = atoi(token.c_str());
                    if (add_num != expected_address[cnt])
                        found_ip = false;
                    cnt++;
                }
            }

            if (found_ip)
                raven_ip = addressBuffer;

        }//  else if (ifa->ifa_addr->sa_family == AF_INET6) { // check it is IP6
        //     // is a valid IP6 Address
        //     tmpAddrPtr=&((struct sockaddr_in6 *)ifa->ifa_addr)->sin6_addr;
        //     char addressBuffer[INET6_ADDRSTRLEN];
        //     inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
        //     printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer);
        // }
    }
    if (ifAddrStruct!=NULL) freeifaddrs(ifAddrStruct);

    if (found_ip)
        printf("\n\n\nFound the acl-raven-ip: %s\n\n\n", raven_ip.c_str());
    else
        printf("\n\n\nNo acl-raven-ip address found\n\n\n");
    return found_ip;
}
