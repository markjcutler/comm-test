/*
 * wifly.cpp
 *
 *  Created on: Dec 22, 2014
 *      Author: mark
 */




#include <iostream>
#include "WiFlyHQ.h"

// acl utils library
#include "acl/serialPort.hpp"
#include "acl/utils.hpp"

// ROS includes
#include "ros/ros.h"

/* Change these to match your WiFi network */
const char mySSID[] = "RAVEN";
const char myPassword[] = "";

void terminal();

WiFly wifly;

int main(int argc, char* argv[])
{

	// initialize ros node
	ros::init(argc, argv, "reply");
	ros::NodeHandle n;

    char buf[32];

    	// setup serial communication
	std::string serialPort = "/dev/ttyUSB5";
	int baudRate = 9600;
	acl::SerialPort ser;
	if (!ser.spInitialize(serialPort.c_str(), baudRate, true))
	{
		ROS_ERROR("Serial port failed to open");
	}

    std::cout << "Starting" << std::endl;
    std::cout << ("Free memory: ");
    std::cout << wifly.getFreeMemory() << std::endl;

    if (!wifly.begin(&wifiSerial, &Serial)) {
        std::cout << "Failed to start wifly" << std::endl;
	terminal();
    }

    if (wifly.getFlushTimeout() != 10) {
        std::cout << "Restoring flush timeout to 10msecs" << std::endl;
        wifly.setFlushTimeout(10);
	wifly.save();
	wifly.reboot();
    }

    /* Join wifi network if not already associated */
    if (!wifly.isAssociated()) {
	/* Setup the WiFly to connect to a wifi network */
	std::cout << "Joining network" << std::endl;
	wifly.setSSID(mySSID);
	wifly.setPassphrase(myPassword);
	wifly.enableDHCP();

	if (wifly.join()) {
	    std::cout << "Joined wifi network" << std::endl;
	} else {
	    std::cout << "Failed to join wifi network" << std::endl;
	    terminal();
	}
    } else {
        std::cout << "Already joined network" << std::endl;
    }

    /* Ping the gateway */
    wifly.getGateway(buf, sizeof(buf));

    std::cout << ("ping ");
    std::cout << (buf);
    std::cout << (" ... ");
    if (wifly.ping(buf)) {
	std::cout << "ok" << std::endl;
    } else {
	std::cout << "failed" << std::endl;
    }

    std::cout << ("ping google.com ... ");
    if (wifly.ping("google.com")) {
	std::cout << "ok" << std::endl;
    } else {
	std::cout << "failed" << std::endl;
    }

    /* Setup for UDP packets, sent automatically */
    wifly.setIpProtocol(WIFLY_PROTOCOL_UDP);
    wifly.setHost("192.168.1.60", 8042);	// Send UDP packet to this server and port

    std::cout << ("MAC: ");
    std::cout << wifly.getMAC(buf, sizeof(buf)) << std::endl;
    std::cout << ("IP: ");
    std::cout << wifly.getIP(buf, sizeof(buf)) << std::endl;
    std::cout << ("Netmask: ");
    std::cout << wifly.getNetmask(buf, sizeof(buf)) << std::endl;
    std::cout << ("Gateway: ");
    std::cout << wifly.getGateway(buf, sizeof(buf)) << std::endl;

    wifly.setDeviceID("Wifly-UDP");
    std::cout << ("DeviceID: ");
    std::cout << wifly.getDeviceID(buf, sizeof(buf)) << std::endl;

    wifly.setHost("192.168.1.60", 8042);	// Send UPD packets to this server and port

    std::cout << "WiFly ready" << std::endl;
}

uint32_t lastSend = 0;
uint32_t count=0;

void loop()
{
    if ((millis() - lastSend) > 1000) {
        count++;
	std::cout << ("Sending message ");
	std::cout << count << std::endl;

	wifly.print("Hello, count=");
	wifly.println(count);
	lastSend = millis();
    }

    if (Serial.available()) {
        /* if the user hits 't', switch to the terminal for debugging */
        if (Serial.read() == 't') {
	    terminal();
	}
    }

}

void terminal()
{
    std::cout << "Terminal ready" << std::endl;
    while (1) {
	if (wifly.available() > 0) {
	    Serial.write(wifly.read());
	}


	if (Serial.available()) {
	    wifly.write(Serial.read());
	}
    }
}
