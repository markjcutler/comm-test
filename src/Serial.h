/*
 * Serial.h
 *
 *  Created on: Jan 5, 2015
 *      Author: mark
 */

#ifndef SERIAL_H_
#define SERIAL_H_

#include "ros/ros.h"
#include "acl/comms/serialPort.hpp"
#include "CommTest.h"

class Serial: public CommTest
{
public:
	Serial(int baudRate, std::string serialPort);
	virtual ~Serial();

	int get_data(uint8_t *, int);
	int send_data(uint8_t *, int);

private:
	acl::SerialPort ser;
};

#endif /* SERIAL_H_ */
