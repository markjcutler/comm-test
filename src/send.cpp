/*
 * comm_test_node.cpp
 *
 *  Created on: Dec 22, 2014
 *      Author: mark
 */

#include <iostream>

// ROS includes
#include "ros/ros.h"

// acl utils library
#include "Serial2.h"
#include "UDP.h"
#include "Bluetooth.h"


int main(int argc, char* argv[])
{
    // initialize ros node
    ros::init(argc, argv, "send", ros::init_options::AnonymousName);
    ros::NodeHandle n;

    // parse the first argument (required)
    if (argc < 2)
    {
        std::cout << "Not enough arguments." << std::endl;
        std::cout << "You must provide at least one indicating which comm"
            "protocol to use (udp, bluetooth, or serial)" << std::endl;
        return -1;
    }

    std::string comm_type = argv[1];

    if (comm_type.compare("udp") == 0 || comm_type.compare("UDP") == 0)
    {
        // default values
        std::string address = "192.168.1.111"; //"127.0.0.1";
        int rxport = 20052;
        int txport = 30052;

        // simple command-line interface for simplified version
        if (argc == 3)
        {
            std::string loopback = argv[2];
            if (loopback.compare("lo") == 0 or loopback.compare("lb") == 0
                or loopback.compare("loopback") == 0)
            {
                address = "127.0.0.1";
            }
            else
            {
                int extension = atoi(argv[2]);
                rxport = 20000 + extension;
                txport = 30000 + extension;
                address = "192.168.1.";
                address.append(argv[2]);
            }
        }

        // alternative command-line values
        if (argc > 4)
        {
            address = argv[2];
            rxport = atoi(argv[3]);
            txport = atoi(argv[4]);
        }

        UDP u(address.c_str(), rxport, txport);
        u.send_packets(0.01, 10000);

    } else if (comm_type.compare("bluetooth") == 0 ||
               comm_type.compare("bt") == 0 ||
               comm_type.compare("BlueTooth") == 0 ||
               comm_type.compare("BT") == 0)
    {

        // default address
        std::string macaddr = "00:06:66:6C:66:A4";

        // alternative command-line values
        if (argc > 2)
        {
            macaddr = argv[2];
        }

        Bluetooth bt(macaddr);
        bt.send_packets(0.1, 1000);

    } else if (comm_type.compare("serial") == 0 ||
               comm_type.compare("xbee") == 0 ||
               comm_type.compare("Serial") == 0 ||
               comm_type.compare("XBEE") == 0 ||
               comm_type.compare("XBee") == 0)
    {
        // default values
        int baudrate = 57600;
        std::string port1 = "/dev/ttyUSB0";
        std::string port2 = "/dev/ttyUSB1";

        // optional commandline inputs
        if (argc > 4)
        {
            baudrate = atoi(argv[2]);
            port1 = "/dev/ttyUSB";
            port1.append(argv[3]);
            port2 = "/dev/ttyUSB";
            port2.append(argv[4]);
        }

        Serial2 s(baudrate, port1, port2);

        if (s.p1_init and s.p2_init)
            s.send_packets(0.1, 100);
    }

    return 1;
}
