/* telnet.cpp
	A simple demonstration telnet client with Boost asio

	Parameters:
		hostname or address
		port - typically 23 for telnet service

	To end the application, send Ctrl-C on standard input
*/

#include <deque>
#include <iostream>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <arpa/inet.h>

using boost::asio::ip::tcp;
using namespace std;

bool get_acl_ip(int expected_address[], int subnet_length,
                std::string &acl_raven_ip)
{
    struct ifaddrs * ifAddrStruct=NULL;
    struct ifaddrs * ifa=NULL;
    void * tmpAddrPtr=NULL;

    getifaddrs(&ifAddrStruct);

    bool found_acl_ip = true;
    printf("\n\n\nLooking for all IP addresses on this machine\n\n");
    printf("WARNING: this script will return the FIRST ip address matching\n");
    printf("    the acl ip address pattern. If there are multiple acl addresses,\n");
    printf("    this script will need to be modified.\n\n\n"); 
    for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
        if (!ifa->ifa_addr) {
            continue;
        }
        if (ifa->ifa_addr->sa_family == AF_INET) { // check it is IP4
            // is a valid IP4 Address
            tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
            char addressBuffer[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
            printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer);

            // parse out the ip4 address into a series of four numbers
            std::string ab(addressBuffer);
            std::istringstream iss(ab);
            std::string token;
            int cnt = 0;
            found_acl_ip = true;
            while (std::getline(iss, token, '.')) {
                if (!token.empty() and cnt < subnet_length)
                {
                    int add_num = atoi(token.c_str());
                    if (add_num != expected_address[cnt])
                        found_acl_ip = false;
                    cnt++;
                }
            }

            if (found_acl_ip)
            {
                acl_raven_ip = addressBuffer;
                break;
            }

        }//  else if (ifa->ifa_addr->sa_family == AF_INET6) { // check it is IP6
        //     // is a valid IP6 Address
        //     tmpAddrPtr=&((struct sockaddr_in6 *)ifa->ifa_addr)->sin6_addr;
        //     char addressBuffer[INET6_ADDRSTRLEN];
        //     inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
        //     printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer);
        // }
    }
    if (ifAddrStruct!=NULL) freeifaddrs(ifAddrStruct);

    if (found_acl_ip)
        printf("\n\n\nFound the acl-raven-ip: %s\n\n\n", acl_raven_ip.c_str());
    else
        printf("\n\n\nNo acl-raven-ip address found\n\n\n");
    return found_acl_ip;
}

class telnet_client
{
public:
	enum { max_read_length = 512 };

	telnet_client(boost::asio::io_service& io_service, tcp::resolver::iterator endpoint_iterator)
		: io_service_(io_service), socket_(io_service)
	{
		connect_start(endpoint_iterator);
	}

	void write(std::string msg, bool terminal)
	{
		if (terminal)
		{
			msg.append("\r\n"); // add the terminal conditions to the string
		}
		std::cout << "Writing: " << msg << std::endl;
		for (int i=0; i<msg.length(); i++)
		{
			write_char(msg[i]);
		}
	}

	void write_char(const char msg) // pass the write_char data to the do_write function via the io service in the other thread
	{
		io_service_.post(boost::bind(&telnet_client::do_write, this, msg));
	}

	void close() // call the do_close function via the io service in the other thread
	{
		io_service_.post(boost::bind(&telnet_client::do_close, this));
	}

private:

	void connect_start(tcp::resolver::iterator endpoint_iterator)
	{ // asynchronously connect a socket to the specified remote endpoint and call connect_complete when it completes or fails
		tcp::endpoint endpoint = *endpoint_iterator;
		socket_.async_connect(endpoint,
			boost::bind(&telnet_client::connect_complete,
				this,
				boost::asio::placeholders::error,
				++endpoint_iterator));
	}

	void connect_complete(const boost::system::error_code& error, tcp::resolver::iterator endpoint_iterator)
	{ // the connection to the server has now completed or failed and returned an error
		if (!error) // success, so start waiting for read data
			read_start();
		else if (endpoint_iterator != tcp::resolver::iterator())
		{ // failed, so wait for another connection event
			socket_.close();
			connect_start(endpoint_iterator);
		}
	}

	void read_start(void)
	{ // Start an asynchronous read and call read_complete when it completes or fails
		socket_.async_read_some(boost::asio::buffer(read_msg_, max_read_length),
			boost::bind(&telnet_client::read_complete,
				this,
				boost::asio::placeholders::error,
				boost::asio::placeholders::bytes_transferred));
	}

	void read_complete(const boost::system::error_code& error, size_t bytes_transferred)
	{ // the asynchronous read operation has now completed or failed and returned an error
		if (!error)
		{ // read completed, so process the data
			cout.write(read_msg_, bytes_transferred); // echo to standard output
			//cout << "\n";
			read_start(); // start waiting for another asynchronous read again
		}
		else
			do_close();
	}

	void do_write(const char msg)
	{ // callback to handle write_char call from outside this class
		bool write_in_progress = !write_msgs_.empty(); // is there anything currently being written?
		write_msgs_.push_back(msg); // store in write_char buffer
		if (!write_in_progress) // if nothing is currently being written, then start
			write_start();
	}

	void write_start(void)
	{ // Start an asynchronous write_char and call write_complete when it completes or fails
		boost::asio::async_write(socket_,
			boost::asio::buffer(&write_msgs_.front(), 1),
			boost::bind(&telnet_client::write_complete,
				this,
				boost::asio::placeholders::error));
	}

	void write_complete(const boost::system::error_code& error)
	{ // the asynchronous read operation has now completed or failed and returned an error
		if (!error)
		{ // write completed, so send next write_char data
			write_msgs_.pop_front(); // remove the completed data
			if (!write_msgs_.empty()) // if there is anthing left to be written
				write_start(); // then start sending the next item in the buffer
		}
		else
			do_close();
	}

	void do_close()
	{
		socket_.close();
	}

private:
	boost::asio::io_service& io_service_; // the main IO service that runs this connection
	tcp::socket socket_; // the socket this instance is connected to
	char read_msg_[max_read_length]; // data read from the socket
	deque<char> write_msgs_; // buffered write_char data
};

int main(int argc, char* argv[])
{
	try
	{
		if (argc != 3)
		{
			cerr << "Usage: telnet <host> <port>\n";
			return 1;
		}
		boost::asio::io_service io_service;
		// resolve the host name and port number to an iterator that can be used to connect to the server
		tcp::resolver resolver(io_service);
		tcp::resolver::query query(argv[1], argv[2]);
		tcp::resolver::iterator iterator = resolver.resolve(query);
		// define an instance of the main class of this program
		telnet_client c(io_service, iterator);
		// run the IO service as a separate thread, so the main thread can block on standard input
		boost::thread t(boost::bind(&boost::asio::io_service::run, &io_service));

		std::string acl_raven_ip;
		int expected_address[3] = {192, 168, 0};

		if (get_acl_ip(expected_address,
								sizeof(expected_address)/sizeof(expected_address[0]),
								acl_raven_ip))
		{

		sleep(1);
		c.write("$$$", 0);
		usleep(300*1000); // sleep for 300 milliseconds
		c.write("set ip host ", 0);
		c.write(acl_raven_ip, 1);
		usleep(20*1000); // sleep for 20 milliseconds
		c.write("save", 1);
		usleep(900*1000); // sleep for 50 milliseconds
		c.write("get ip", 1);
		usleep(900*1000);
		c.write("exit", 1);
		}
		else
		{
			std::cout << "ERROR: could not find ACL RAVEN ip addres" << std::endl;
		}

		c.close(); // close the telnet client connection
		t.join(); // wait for the IO service thread to close
	}
	catch (exception& e)
	{
		cerr << "Exception: " << e.what() << "\n";
	}
	return 0;
}

