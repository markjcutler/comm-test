/*
 * reply.cpp
 *
 *  Created on: Dec 22, 2014
 *      Author: mark
 */


#include <iostream>
#include "ros/ros.h"

// acl utils library
#include "Serial2.h"
#include "UDP.h"

#include "mavlink_msgs/acl_msgs/mavlink.h"

#define BUFFER_LENGTH 2048

int main(int argc, char* argv[])
{

    // initialize ros node
    ros::init(argc, argv, "reply", ros::init_options::AnonymousName);
    ros::NodeHandle n;

    // parse the first argument (required)
    if (argc < 2)
    {
        std::cout << "Not enough arguments." << std::endl;
        std::cout << "You must provide at least one indicating which comm"
            "protocol to use (udp, bluetooth, or serial)" << std::endl;
        return -1;
    }

    std::string comm_type = argv[1];

    if (comm_type.compare("udp") == 0 || comm_type.compare("UDP") == 0)
    {
        // default values
        std::string address = "192.168.1.222"; //"127.0.0.1";
        int rxport = 30052;
        int txport = 20052;

        // simple command-line interface for simplified version
        if (argc == 3)
        {
            std::string loopback = argv[2];
            if (loopback.compare("lo") == 0 or loopback.compare("lb") == 0
                or loopback.compare("loopback") == 0)
            {
                address = "127.0.0.1";
            }
            else
            {
                int extension = atoi(argv[2]);
                rxport = 20000 + extension;
                txport = 30000 + extension;
                address = "192.168.1.";
                address.append(argv[2]);
            }
        }

        // alternative command-line values
        if (argc > 4)
        {
            address = argv[2];
            rxport = atoi(argv[3]);
            txport = atoi(argv[4]);
        }

        UDP u(address.c_str(), rxport, txport);
        u.reply_packets();
    }
    else if (comm_type.compare("serial") == 0 ||
             comm_type.compare("xbee") == 0 ||
             comm_type.compare("Serial") == 0 ||
             comm_type.compare("XBEE") == 0 ||
             comm_type.compare("XBee") == 0)
    {
        // default settings
        int baudRate = 57600;
        std::string serialPort1 = "/dev/ttyUSB0";
        std::string serialPort2 = "/dev/ttyUSB0"; // default to 1-way comms
        bool two_dir = false;

        // parse inputs
        if (argc == 4)
        {
            baudRate = atoi(argv[1]);
            serialPort1 = "/dev/ttyUSB";
            serialPort1.append(argv[2]);
            serialPort2 = "/dev/ttyUSB";
            serialPort2.append(argv[2]);
        }
        if (argc == 5)
        {
            two_dir = true;
            baudRate = atoi(argv[1]);
            serialPort1 = "/dev/ttyUSB";
            serialPort1.append(argv[2]);
            serialPort2 = "/dev/ttyUSB";
            serialPort2.append(argv[3]);
        }

        uint8_t buf[BUFFER_LENGTH];
        ssize_t recsize;
        socklen_t fromlen;
        int bytes_sent;
        mavlink_message_t msg;
        uint16_t len;

        // setup serial communication
        acl::SerialPort ser1, ser2;
        if (!ser1.spInitialize(serialPort1.c_str(), baudRate, false))
        {
            std::cout << "Serial port failed to open" << std::endl;
            return 0;
        }
        std::cout << "Serial port " << serialPort1 << " started at " << baudRate << " bits/sec\n" << std::endl;
        if (two_dir)
        {
            if (!ser2.spInitialize(serialPort2.c_str(), baudRate, false))
            {
                std::cout << "Serial port failed to open" << std::endl;
                return 0;
            }
            std::cout << "Serial port " << serialPort2 << " started at " << baudRate << " bits/sec\n" << std::endl;
        }

        Serial2 s(baudRate, serialPort1, serialPort2);
        if (s.p1_init and s.p2_init)
            s.reply_packets();
    }

    return 1;
}
