/*
 * UDP.h
 *
 *  Created on: Jan 5, 2015
 *      Author: mark
 */

#ifndef UDP_H_
#define UDP_H_

#include "CommTest.h"
#include "ros/ros.h"
#include "acl/comms/udpPort.hpp"
#include <sstream>
#include <stdio.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>


class UDP: public CommTest
{
public:
    UDP(const char *address, int rxport, int txport);
    virtual ~UDP();

    int get_data(uint8_t *, int);
    int send_data(uint8_t *, int);

private:
    acl::UDPPort udp;
    bool get_ip(int expected_address[], int subnet_length,
                     std::string &raven_ip);
    bool set_ip_on_wifly(std::string ip_address);
    std::string address;
    int rxport, txport;
};

#endif /* UDP_H_ */
