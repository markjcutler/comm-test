/*
 * setup_rn_xv.cpp
 *
 *  Created on: Mar 6, 2015
 *      Author: mark
 *
 *  This is a simple utility to setup a new Roving Networks
 *    RN-XV wifi xbee module.  See the comm-test wiki on Bitbucket
 *    for more information.
 */

#include <iostream>
#include <string>

// acl utils library
#include "acl/comms/serialPort.hpp"


void *listen(void *s)
{
    acl::SerialPort *ser = (acl::SerialPort*) s;

    while (1)
    {

        // listen to serial port
        uint8_t c  = ser->spReceiveSingle();
        printf("%c",c);
    }
    return NULL;
}



int main(int argc, char* argv[])
{

    // setup configuration variables
    std::string ssid = "RAVEN_24";
    std::string remote = "20057";
    std::string local = "30057";
    // std::string address = "192.167.2.100";
    // std::string address = "192.168.0.19";
    std::string address = "192.168.0.21";
    std::string phrase = "uavswarm";
    std::string serialPort = "/dev/ttyUSB0";
    int baudRate = 115200;
    // int baudRate = 115200;
    // int baudRate = 57600;

    acl::SerialPort ser;

    // setup serial port
    if (!ser.spInitialize(serialPort.c_str(), baudRate, true))
    {
        std::cout << "Serial port failed to open" << std::endl;
        return 0;
    }


    // setup listen thread
    pthread_t threads;
    if (pthread_create(&threads, NULL, listen, &ser))
        std::cout << "Serial listen thread failed to start" << std::endl;


    char com[100];
    int n = sprintf(com,"%s","$$$");
    ser.spSend(com, n);
    sleep(1);

    n = sprintf(com,"%s","factory RESET\r\n");
    ser.spSend(com, n);

    n = sprintf(com,"%s","save\r\n");
    ser.spSend(com, n);

    n = sprintf(com,"%s","reboot\r\n");
    ser.spSend(com, n);
    sleep(1);

    // close serial port and reopen at 9600
    ser.spClose();
    // setup serial port
    if (!ser.spInitialize(serialPort.c_str(), 9600, true))
    {
        std::cout << "Serial port failed to open" << std::endl;
        return 0;
    }

    n = sprintf(com,"%s","$$$");
    ser.spSend(com, n);
    sleep(1);


    n = sprintf(com,"%s","set wlan auth 4\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com,"%s","set wlan join 1\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com,"%s%s%s","set wlan ssid ", ssid.c_str(), "\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com,"%s%s%s","set wlan phrase ", phrase.c_str(), "\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com,"%s","set ip protocol 3\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com,"%s%s%s","set ip host ", address.c_str(), "\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com,"%s%s%s","set ip remote ", remote.c_str(), "\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com,"%s%s%s","set ip localport ", local.c_str(), "\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com,"%s%s%s","join ", ssid.c_str(), "\r\n");
    ser.spSend(com, n);
    sleep(1);
    //n = sprintf(com,"%s","set u b 115200\r\n");
    n = sprintf(com,"%s","set u b 57600\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com,"%s","save\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com,"%s","reboot\r\n");
    ser.spSend(com, n);
    sleep(1);


    return 1;
}
